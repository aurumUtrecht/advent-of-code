from day4 import BingoBoard
import numpy as np
import pandas as pd


def test_bingo():
    input = pd.DataFrame([[22, 13, 17, 11,  0],
                          [8,  2, 23,  4, 24],
                          [21,  9, 14, 16,  7],
                          [6, 10,  3, 18,  5],
                          [1, 12, 20, 15, 19]])

    board = BingoBoard(input)
    bingo = False
    for number in [7,4,9,5,11,17,23,2,0,14,21,24,10,16]:
        if board.draw_number(number):
            bingo = True
    assert bingo

