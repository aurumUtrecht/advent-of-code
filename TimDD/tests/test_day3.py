from day3 import run_part2_1, run_part2_2
import numpy as np


def test_day2_1():
    data = np.array([[1,0,0],[0,1,0],[1,1,1]])
    result = run_part2_1(data)
    assert result == 7


def test_day2_2():
    data = np.array([[1, 0, 0], [1, 1, 0], [0, 1, 1], [0, 0, 1]])
    result = run_part2_2(data)
    assert result == 1
