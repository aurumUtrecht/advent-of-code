import numpy as np
import pandas as pd
from load_data import load_data


class BingoBoard:
    def __init__(self, grid: pd.DataFrame):
        self.grid = grid
        self.marked = pd.DataFrame(np.zeros((5, 5)))

    def draw_number(self, number: int):
        self.marked.iloc[np.where(self.grid == number)] = 1
        return self.check_bingo()

    def check_bingo(self):
        if np.any(self.marked.sum(axis=0).isin([5])) or np.any(self.marked.sum(axis=1).isin([5])):
            return True
        return False

    def get_result(self):
        return self.grid.where(self.marked == 0).sum().sum()


def draw_numbers(boards, numbers):
    for number in numbers:
        print("drawing: ", number)
        tmp_boards = boards.copy()
        for board_id, board in tmp_boards.items():
            if board.draw_number(int(number)):
                # return board.get_result()*int(number)      # Part 1
                if len(boards) == 1:                         # part 2
                    return board.get_result() * int(number)  # part 2
                boards.pop(board_id)                         # part 2


def run_step_1(data, drawing):
    boards = []
    for i in range(2,len(data),6):
        boards.append(BingoBoard(pd.DataFrame(line.split() for line in data[i:i+5]).astype(int)))
    print(draw_numbers(dict(enumerate(boards)),drawing))


if __name__ == "__main__":
    data = load_data.load(4)
    drawing = data[0].split(sep=',')
    run_step_1(data, drawing)
