from typing import Tuple
import numpy as np
from load_data import load_data


def calculate_line(line: str) -> np.ndarray:
    direction, amount = line.split()
    amount = int(amount)
    calculation_dict = {"up": (-1,0),"down": (1,0), "forward": (0,1)}
    return np.array(calculation_dict[direction])*amount


depth, horizontal_position = 0, 0

for line in load_data.load(2):
    new_depth , new_horizontal_position = calculate_line(line)
    depth += new_depth
    horizontal_position += new_horizontal_position

print(depth * horizontal_position)


# part two. helaas is mijn code nét kut om voor deel twee te gebruiken. dan maar een hack.


depth, horizontal_position, aim = 0, 0, 0


for line in load_data.load(2):
    direction, amount = line.split()
    amount = int(amount)
    if direction == "down":
        aim += amount
    elif direction == "up":
        aim -= amount
    elif direction == "forward":
        horizontal_position += amount
        depth += aim * amount
    else:
        raise SyntaxError("dude wtf klopt niet")
print(horizontal_position*depth)




