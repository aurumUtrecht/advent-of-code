from load_data import load_data
import re
import numpy as np

iterations = 40

data = load_data.load(14, False)

input_string = data[0]

pairs = {}
pair_insertion = {}

for i, line in enumerate(data[2:]):
    template = re.search(r"(..) -> (.)", line)
    pairs[template.group(1)] = i
    pair_insertion[template.group(1)] = [template.group(1)[0] + template.group(2),
                                         template.group(2) + template.group(1)[1]]

matrix = np.zeros((len(pairs), len(pairs)))

for pair in pair_insertion:
    x = pairs[pair]
    y = pair_insertion[pair]
    matrix[x, pairs[y[0]]] = 1
    matrix[x, pairs[y[1]]] = 1

edges = input_string[0], input_string[-1]

input_polymers = np.zeros(len(pairs))
for i in range(len(input_string)-1):
    input_polymers[pairs[input_string[i:i+2]]] += 1

for i in range(iterations):
    input_polymers = input_polymers @ matrix
print(input_polymers)

results = {}

for pair in pairs:
    amount = input_polymers[pairs[pair]]
    for letter in pair:
        try:
            results[letter] += amount
        except KeyError:
            results[letter] = amount

results[edges[0]] += 1
results[edges[1]] += 1

results = np.array(list(results.values()))/2

print(results.max())
print(results.min())
print(results.max()-results.min())
