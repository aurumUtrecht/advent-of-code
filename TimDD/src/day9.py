from load_data import load_data
import pandas as pd
import numpy as np


def is_lowest(map, position_x, position_y):
    position = map[position_x, position_y]
    max_x = np.shape(map)[0]-1
    max_y= np.shape(map)[1]-1
    if not position_x == 0:
        if map[position_x - 1, position_y] <= position:
            return False
    if not position_x == max_x:
        if map[position_x + 1, position_y] <= position:
            return False
    if not position_y == 0:
        if map[position_x, position_y - 1] <= position:
            return False
    if not position_y == max_y:
        if map[position_x, position_y + 1] <= position:
            return False
    return True


data = load_data.load(9, False)
height = len(data)
breadth = len(data[0])
map = np.zeros((height,breadth))
for height, line in enumerate(data):
    for breadth, char in enumerate(line):
        map[height, breadth] = int(char)

low_points = []
for i in range(np.shape(map)[0]):
    for j in range(np.shape(map)[1]):
        if is_lowest(map, i,j):
            low_points.append([i,j])

print(low_points)


def find_edges(map, position, basin):
    i,j = position
    max_x = np.shape(map)[0]-1
    max_y = np.shape(map)[1]-1
    if not position[0] == 0:
        if [i-1,j] not in basin and map[position[0] - 1, position[1]] != 9:
            basin.append([i-1,j])
            basin = find_edges(map,[i-1,j],basin)
    if not position[0] == max_x:
        if [i+1,j] not in basin and map[position[0] + 1, position[1]] != 9:
            basin.append([i+1,j])
            basin = find_edges(map,[i+1,j],basin)
    if not position[1] == 0:
        if [i,j-1] not in basin and map[position[0], position[1] - 1] != 9:
            basin.append([i,j-1])
            basin = find_edges(map,[i,j-1],basin)
    if not position[1] == max_y:
        if [i,j+1] not in basin and map[position[0], position[1] + 1] != 9:
            basin.append([i,j+1])
            basin = find_edges(map,[i,j+1],basin)
    return basin


sizes = []
for low_point in low_points:
    input_basin = [low_point.copy()]
    sizes.append(len(find_edges(map,low_point,input_basin)))
    print(sizes)
sizes_sorted = np.sort(sizes)
print(sizes_sorted)
print(sizes_sorted[-1]*sizes_sorted[-2]*sizes_sorted[-3])