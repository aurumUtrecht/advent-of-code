import numpy as np
from load_data import load_data


def triangular_number(input):
    return input *(input+1) / 2


data = load_data.load(7, False)
positions = np.array(data[0].split(',')).astype(int)


result_dict={}
for target in range(max(positions)):
    result_dict[target] = np.sum(triangular_number(np.abs(target-positions)))

result_position = min(result_dict, key=result_dict.get)
print(result_position, result_dict[result_position])

