from load_data import load_data
import numpy as np
from itertools import filterfalse
from collections import Counter


def move(routes, connections):
    new_routes = []
    for route in routes:
        next_steps = connections[route[-1]]
        new_routes_for_old_route = []
        for next_step in next_steps:
            new_routes_for_old_route.append(route + [next_step])
        new_routes = new_routes + new_routes_for_old_route
    # god damn deel 2 is going to be a mess ?
    # non_repeated_paths = [path for path in new_routes if not path[-1] in path[0:-1] or path[-1].isupper()]

    def illegal_path(path):
        if path[-1].isupper():
            return False
        elif path[-1] == 'start':
            return True
        elif path[-1] in path[0:-1]:
            lowercase_caves = [cave for cave in path[0:-1] if cave.islower()]
            duplicate_caves = Counter(lowercase_caves)
            if max(duplicate_caves.values()) > 1:
                return True
        else:
            return False

    non_repeated_paths = [*filterfalse(illegal_path, new_routes)]

    return non_repeated_paths


data = load_data.load(12)

connections = {}
for line in data:
    caves = line.split('-')
    for (cave1, cave2) in [caves, reversed(caves)]:
        if cave1 in connections.keys():
            connections[cave1].append(cave2)
        else:
            connections[cave1] = [cave2]

routes = [['start']]

finished_paths = 0
while True:
    new_routes = move(routes, connections)
    routes = [path for path in new_routes if not path[-1] == 'end']
    finished_paths += len(new_routes) - len(routes)
    if len(routes) == 0:
        break
print(finished_paths)

