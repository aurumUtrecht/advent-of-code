from load_data import load_data


def syntax_check(line, brackets):
    currently_open = []
    for char in line:
        if char in brackets.values():
            currently_open.append(char)
        elif char in brackets.keys():
            if currently_open[-1] == brackets[char]:
                currently_open.pop()
            else:
                return False, char, None
    return True, None, currently_open


data = load_data.load(10, False)

brackets = {']': '[', '}': '{', ')': '(', '>': '<'}
brackets_score = {']': 57, '}': 1197, ')': 3, '>': 25137}
brackets_score_part_2 = {'[': 2, '{': 3, '(': 1, '<': 4}
multiplier = 5
score_part_1 = 0
score_part_2 = []
for line in data:
    success, char, open_brackets = syntax_check(line, brackets)
    # part 1
    if not success:
        score_part_1 += brackets_score[char]
    # part 2
    if success:
        line_score = 0
        for bracket in reversed(open_brackets):
            line_score = line_score * 5 + brackets_score_part_2[bracket]
        score_part_2.append(line_score)
print(score_part_1)
score_part_2.sort()
middle = int((len(score_part_2)-1)/2)
print(score_part_2[middle])

