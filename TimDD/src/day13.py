from load_data import load_data
import numpy as np
import re
import matplotlib.pyplot as plt


def fold(array: np.ndarray, axis, value):
    for i in range(array.shape[0]):
        if array[i, axis] > value:
            array[i, axis] = 2 * value - array[i, axis]
    return np.unique(array, axis=0)


data = load_data.load(13, False)
split_list_index = data.index("")
dots_str = data[:split_list_index]
instructions_str = data[split_list_index + 1:]

dots = np.array([*(tuple(dot.split(',')) for dot in dots_str)]).astype(int)
convert_axis = {"x": 0, "y": 1}
for folding in instructions_str:
    axis_and_value = re.search(r"fold along ([xy])=(\d*)", folding)
    dots = fold(dots, convert_axis[axis_and_value.group(1)], int(axis_and_value.group(2)))

plt.scatter(dots[:,0], -dots[:,1])
plt.show()