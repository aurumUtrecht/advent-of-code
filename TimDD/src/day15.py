from load_data import load_data
import numpy as np
import matplotlib.pyplot as plt


class DepthFirst:

    def __init__(self):
        self.limit = np.inf

    @staticmethod
    def array_in_list_of_arrays(arr, arr_list):
        for a in arr_list:
            if arr[0] == a[0] and arr[1] == a[1]:
                return True
        return False

    def step(self, field, location, path, value):
        result_path = []
        result = np.inf

        if location[0] == field.shape[0]-1 and location[1] == field.shape[1]-1:
            print(value, path, value)
            return path, value

        if value + (field.shape[0]-location[0]) + (field.shape[1]-location[1])> self.limit:
            # print('too high:', value, path)
            return path, np.inf
        # for nextstep in [np.array([1,0]),np.array([0,1]),np.array([-1,0]),np.array([0,-1])]:
        for nextstep in [np.array([1,0]),np.array([0,1])]:
            newloc = location + nextstep
            if not self.array_in_list_of_arrays(newloc, path) and all(0<=x<field.shape[0] for x in newloc):
                newpath = path.copy()
                newval = value + field[newloc[0], newloc[1]]
                newpath.append(newloc)
                newpath, res = self.step(field, newloc, newpath, newval)
                if res < self.limit:
                    self.limit = res
                if res < result:
                    result = res
                    result_path = newpath
        return result_path, result


data = load_data.load(15)

field = np.array([[char for char in line] for line in data]).astype(int)

path, value =DepthFirst().step(field, np.array([0,0]),[np.array([0,0])],0)
print("Final result:")
print(path)
print(value)

plt.scatter([x[0] for x in path], [x[1] for x in path])
plt.show()