from load_data import load_data
import numpy as np
from scipy import ndimage


def step(padded_data: np.ndarray):
    full_trigger = False
    padded_data = padded_data + 1
    previously_triggered = np.zeros(np.shape(padded_data))
    while True:
        triggered = padded_data > 9
        newly_triggered = np.logical_xor(triggered, previously_triggered)
        previously_triggered = triggered
        if not np.any(newly_triggered):
            break
        flash_increase = ndimage.convolve(newly_triggered.astype(int), np.ones((3, 3)))
        padded_data = padded_data + flash_increase
    flashed_count = np.sum(triggered)
    if flashed_count == 100:
        full_trigger = True
    padded_data[triggered] = 0
    return padded_data, flashed_count, full_trigger


data = load_data.to_numpy_map(11, False)

padded = np.pad(data, ((1,1),(1,1)), 'constant', constant_values=-10**3)
score = 0
i = 1
while(True):
    padded, count, full_trigger = step(padded)
    score += count
    if full_trigger:
        print(i)
        break
    i += 1
print(score)
