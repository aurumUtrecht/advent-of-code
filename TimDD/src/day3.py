import numpy as np
from load_data import load_data


def array_to_binary(array):
    output = ""
    for bit in array:
        output += str(int(bit))
    return int(output, 2)


def calculate_gamma_epsilon(data):
    gamma = np.sum(data, axis=0) >= np.sum(1-data, axis=0)
    epsilon = np.sum(data, axis=0) < np.sum(1-data, axis=0)
    return gamma, epsilon


def run_part1(data):
    data = np.array([[int(char) for char in line] for line in load_data.load(3)])

    gamma, epsilon = calculate_gamma_epsilon(data)
    gamma_binary = array_to_binary(gamma)
    epsilon_binary = array_to_binary(epsilon)
    epsilon_otherwise = 2**12-1-gamma_binary
    assert(epsilon_binary == epsilon_otherwise) # leuk, dat werkt ook
    print(gamma_binary * epsilon_binary)


# Part 2
def run_part2_1(data):
    checking_int = 0
    while True:
        gamma, epsilon = calculate_gamma_epsilon(data)
        data = data[data[:,checking_int] == gamma[checking_int]]
        if data.shape[0] == 1:
            return int(array_to_binary(data.flatten()))
        checking_int += 1


def run_part2_2(data):
    checking_int = 0
    while True:
        gamma, epsilon = calculate_gamma_epsilon(data)
        data = data[data[:,checking_int] == epsilon[checking_int]]
        if data.shape[0] == 1:
            return int(array_to_binary(data.flatten()))
        checking_int += 1


if __name__ == "__main__":
    data = np.array([[int(char) for char in line] for line in load_data.load(3)])
    run_part1(data)
    oxygen_rating = run_part2_1(data)
    co2_rating = run_part2_2(data)

    print(oxygen_rating, co2_rating, oxygen_rating*co2_rating)
