import numpy as np

def load(day, test=False):
    filename = "..//data//day" + str(day) + ".txt"
    if test==True:
        filename = "..//data//day" + str(day) + "test.txt"
    with open(filename, "r") as input:
        data = input.read().splitlines()
    return data


def to_numpy_map(day, test=False):
    data = load(day,test)
    height = len(data)
    breadth = len(data[0])
    map = np.zeros((height, breadth))
    for height, line in enumerate(data):
        for breadth, char in enumerate(line):
            map[height, breadth] = int(char)
    return map
