import numpy as np
import re
from load_data import load_data


def add_line_to_field(line_x0, line_y0, line_x1, line_y1, coordinate_field):
    if line_x0 == line_x1:
        for y in range(min(line_y0, line_y1), max(line_y0,line_y1)+1):
            coordinate_field[line_x0,y] += 1
    elif line_y0 == line_y1:
        for x in range(min(line_x0, line_x1), max(line_x0,line_x1)+1):
            coordinate_field[x, line_y0] += 1
    else:
        direction_x = np.sign(line_x1-line_x0)
        direction_y = np.sign(line_y1-line_y0)
        for step in range(np.abs(line_x1-line_x0)+1):
            coordinate_field[line_x0+step*direction_x, line_y0+step*direction_y] +=1



if __name__ == "__main__":
    data = load_data.load(5,test=False)
    segments = []
    for line in data:
        segments += [re.findall(r'\d+',line)]

    size_of_field = max(int(item) for sublist in segments for item in sublist)+1
    coordinate_field = np.zeros((size_of_field,size_of_field))
    for segment in segments:
        add_line_to_field(int(segment[0]),int(segment[1]),int(segment[2]),int(segment[3]),coordinate_field)
    print(coordinate_field.transpose())
    print("result: " + str(np.sum(coordinate_field > 1)))


