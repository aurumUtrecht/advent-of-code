import numpy as np
from load_data import load_data


def breed_fish(fishes, store_intermediates):
    fish_calculations = {}
    for day in range(128):
        unique, counts = np.unique(fishes, return_counts=True)
        count_dict=dict(zip(unique,counts))
        fishes=fishes-1
        fishes[np.where(fishes==-1)] = 6
        try:
            fishes = np.append(fishes,np.full(count_dict[0],8))
        except:
            pass
        if store_intermediates:
            fish_calculations[day+1] = len(fishes)
    if store_intermediates:
        return fish_calculations
    else:
        return fishes


data = load_data.load(6)
fishes = np.array(data[0].split(',')).astype(int)

growth_data = breed_fish(np.array([0]), True)
population_day_128 = breed_fish(fishes, False)

unique, counts = np.unique(population_day_128, return_counts=True)
fish_dict = dict(zip(unique, counts))
final_fishes = 0
for timer in range(9):
    final_fishes +=int(fish_dict[0+timer])*int(growth_data[128-timer])

print(final_fishes)