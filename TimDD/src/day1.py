import numpy as np
from load_data import load_data

data = np.array(load_data.load(1), dtype=int)
compare_data = np.insert(np.delete(data.copy(), -1),0,0)
increase = data > compare_data

print("answer 1")
print(sum(increase))


compare_data_further = np.insert(np.delete(compare_data.copy(), -1),0,0)
summed = data + compare_data + compare_data_further
summed2 = np.insert(np.delete(summed.copy(), -1),0,0)
new_increase = summed > summed2

print("answer 2:")
print(sum(new_increase)-3)