package windmill;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import utils.FileReaderHelper;

public class windmill {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2021/windmill");
		List<Team> teams= new ArrayList<>();
		
		

		for (int i = 1; i < list.size(); i++) {
			 Player player = createPlayer(list.get(i));
			 
			 boolean playeradded = false;
			 
			 for (Team team : teams) {
				if( team.getTeamname().equals(player.getTeamname())) {
					team.addplayer(player);
					playeradded = true;
					continue;
				}
			}
			 
			 if (!playeradded) {
				 Team team = new Team(player.getTeamname());
				 team.addplayer(player);
				 teams.add(team);
			 }
		}
		
		
		for (int i = 130 ; i < teams.size() ;i++ ) {
			
			System.out.println("-------------------------------------------");
			Team team = teams.get(i);
			System.out.println(team.getTeamname());
			System.out.println(list.get(0));
			for (Player player : team.getPlayers()) {
				System.out.println(player);
			}
		}

		
		
	}

	private Player createPlayer(String line) {
		String[] parts = line.split(";");
		String[] relation = parts[8].replace("\"", "").replace("{", "").replace("}", "").split(",");
		
		Player player = new Player(parts[1], relation[3].split(":")[2], relation[2].split(":")[1], parts[0], parts[2], parts[3], parts[4], 
				parts[5], parts[6], parts[7]);
		
		return player;
	}

	
	public static void main(String[] args) throws IOException {
		windmill wm = new windmill();
		wm.run();
	}
	
	private class Team {
		String teamname;
		List<Player> players = new ArrayList<>();
		
		public Team(String teamname) {
			this.teamname = teamname;
		}
		
		public String getTeamname() {
			return teamname;
		}
		
		public List<Player> getPlayers() {
			Comparator<Player> comparator = new Comparator<Player>() {

		        @Override
		        public int compare(Player p1, Player p2) {
		        	return extractInt(p1.shirtNumber) - extractInt(p2.shirtNumber);
		        	
		        }

				private int extractInt(String s) {
					String num = s.replaceAll("\\D", "");
			        // return 0 if no digits found
			        return num.isEmpty() ? 0 : Integer.parseInt(num);
				}
		              };
			
			
			
			Collections.sort(players, comparator);
			return players;
		}
		
		
		public void addplayer(Player player) {
			players.add(player);
		}
	}
	
	

	private class Player {

		String shirtNumber;
		String name;
		String teamname;
		String nickname;
		String funFact;
		String job;
		String fieldPosition;
		String line;
		String yearOfExperience;
		String role;

		public Player(String shirtNumber, String name, String teamname, String nickname, String funFact, String job,
				String fieldPosition, String line, String yearOfExperience, String role) {
			super();
			this.shirtNumber = shirtNumber;
			this.name = name;
			this.teamname = teamname;
			this.nickname = nickname;
			this.funFact = funFact;
			this.job = job;
			this.fieldPosition = fieldPosition;
			this.line = line;
			this.yearOfExperience = yearOfExperience;
			this.role = role;
		}

		public String getShirtNumber() {
			return shirtNumber;
		}

		public String getName() {
			return name;
		}

		public String getTeamname() {
			return teamname;
		}

		public String getNickname() {
			return nickname;
		}

		public String getFunFact() {
			return funFact;
		}

		public String getJob() {
			return job;
		}

		public String getFieldPosition() {
			return fieldPosition;
		}

		public String getLine() {
			return line;
		}

		public String getYearOfExperience() {
			return yearOfExperience;
		}

		public String getRole() {
			return role;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(shirtNumber);
			sb.append(", ");
			sb.append(name);
			sb.append(", ");
			sb.append(nickname);
			sb.append(", ");
			sb.append(role);
			sb.append(", ");
			sb.append(line);
			sb.append(", ");
			sb.append(fieldPosition);
			sb.append(", ");
			sb.append(yearOfExperience);
			sb.append(", ");
			sb.append(job);
			sb.append(", ");
			sb.append(funFact);
			return sb.toString();
		}
		
	}
	
}
