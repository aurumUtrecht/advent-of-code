package days2019;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

public final class Day5 {

	private void run() throws IOException {
	List<Integer> list = FileReaderHelper.readFileToInt("input2019/day5");
		
		Intprocessor processor = new Intprocessor(list);
		processor.printList();
		processor.run();	
		
	}
	
	public static void main(String[] args) throws IOException {
		Day5 day5 = new Day5();
		day5.run();
	}
	
}
