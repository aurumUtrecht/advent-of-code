package days2019;

import java.util.ArrayList;
import java.util.List;

public final class Day4 {

	private static int RANGE_START = 235741;
	private static int RANGE_END = 706948;
	private static int PASSWORD_LENGTH = 6;

	private void run() {

		int possiblePasswords = 0;

		for (int i = RANGE_START; i <= RANGE_END; i++) {
			if (meetsCriteria(i)) {
				possiblePasswords++;
			}

		}

		System.out.println(possiblePasswords);

	}

	private boolean meetsCriteria(int password) {

		return isSixDigitNumber(password) && isWithinRange(password) && twoAdjecentDigitsAreEqual(password)
				&& digitsNeverDecrease(password) && hasExactTwoAdjecingNumbers(password);
	}

	private boolean isSixDigitNumber(int password) {
		int length = (int) (Math.log10(password) + 1);
		return length == PASSWORD_LENGTH;
	}

	private boolean isWithinRange(int password) {
		return password >= RANGE_START && password <= RANGE_END;
	}

	private boolean twoAdjecentDigitsAreEqual(int password) {
		List<Integer> numbers = getNumbers(password);

		for (int i = 0; i < numbers.size() - 1; i++) {
			if (numbers.get(i) == numbers.get(i + 1)) {
				return true;
			}
		}
		return false;
	}

	private boolean digitsNeverDecrease(int password) {
		List<Integer> numbers = getNumbers(password);
		
		int previousNumber = numbers.get(0);
		
		for (int i = 1; i < numbers.size(); i++) {
			if (numbers.get(i) < previousNumber) {
				return false;
			}
			previousNumber = numbers.get(i);
		}
		
		return true;
	}
	
	private boolean hasExactTwoAdjecingNumbers(int password) {
		List<Integer> numbers = getNumbers(password);
		
		boolean doubleFound = false;
		
		int currentNumber = 1000; // non possible number to make code easier
		int numberCount = 1;
		
		for (Integer number : numbers) {
			if (number != currentNumber) {
				
				if (numberCount == 2) {
					doubleFound = true;
				}
				
				currentNumber = number;
				numberCount = 1;
				continue;
			}
			
			// same number
			numberCount++;
		}
		
		if (numberCount == 2) {
			doubleFound = true;
		}
		
		return doubleFound;
	}
	
	
	private List<Integer> getNumbers(int password) {
		List<Integer> numbers = new ArrayList<>();

		int passwordRemaining = password;

		while (passwordRemaining > 0) {
			numbers.add(0, passwordRemaining % 10);
			passwordRemaining = passwordRemaining / 10;
		}
		return numbers;
	}

	public static void main(String[] args) {
		Day4 day4 = new Day4();
		day4.run();
	}

}
