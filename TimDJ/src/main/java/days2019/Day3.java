package days2019;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import utils.FileReaderHelper;

public final class Day3 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2019/day3");

		List<Point> lineA = lineToList(list.get(0));
		List<Point> lineB = lineToList(list.get(1));

		List<MatchingPoints> matchingPoints = findMatchingPoints(lineA, lineB);

		System.out.println(lineA.size());
		System.out.println(lineB.size());
		System.out.println(matchingPoints.size());

		System.out.println("matches:");
		for (MatchingPoints point : matchingPoints) {
			System.out.println(point.getPointA().getX() + " - " + point.getPointA().getY());
		}

		int lowestDistance = findLowestDistance(matchingPoints);
		int shortestDistance = findShortestDistance(matchingPoints, lineA, lineB);
		System.out.println("lowest distance: " + lowestDistance);
		System.out.println("shortest distance: " + shortestDistance);
	}

	private int findShortestDistance(List<MatchingPoints> matchingPoints, List<Point> lineA, List<Point> lineB) {
		int shortestDistance = Integer.MAX_VALUE;
		
		for (MatchingPoints point : matchingPoints) {
			int a = lineA.indexOf(point.getPointA());
			int b = lineB.indexOf(point.getPointB());
			
			// As the index is 1 lower than the the steps it takes
			int distance = a + b + 2;
			if (distance > 0 && distance < shortestDistance) {
				shortestDistance = distance;
			}
			
		}
		return shortestDistance;
	}

	private int findLowestDistance(List<MatchingPoints> matchingPoints) {
		int lowestDistance = Integer.MAX_VALUE;

		for (MatchingPoints point : matchingPoints) {
			int distance = Math.abs(point.getPointA().getX()) + Math.abs(point.getPointA().getY());
			if (distance > 0 && distance < lowestDistance) {
				lowestDistance = distance;
			}
		}
		return lowestDistance;
	}

	private List<MatchingPoints> findMatchingPoints(List<Point> lineA, List<Point> lineB) {
		List<MatchingPoints> matching = new ArrayList<>();

		long i = 0;
		int j = 0;

		for (Point point1 : lineA) {
			for (Point point2 : lineB) {
				if (point1.getY() == point2.getY() && point1.getX() == point2.getX()) {
					matching.add(new MatchingPoints(point1, point2));
					System.out.println(point1.getX() + " " + point1.getY());
				}
				i++;
			}
			if (j % 2500 == 0) {
				System.out.println(String.format("compared %d points", i));
			}
			j++;
		}
		return matching;
	}

	private List<Point> lineToList(String line) {
		List<Point> list = new ArrayList<>();

		String[] splitLines = line.split(",");

		Point previousPoint = new Point(0, 0);

		for (String string : splitLines) {
			char direction = string.charAt(0);
			int distance = Integer.valueOf(string.substring(1));

			Result result = createLine(direction, distance, previousPoint);
			
			list.addAll(result.getLine());
			previousPoint = result.getNewPoint();	
		}

		return list;
	}

	private Result createLine(char direction, int distance, Point previousPoint) {
		int x = previousPoint.getX();
		int y = previousPoint.getY();
		int newX = x;
		int newY = y;

		switch (direction) {
		case 'U':
			newY = y + distance;
			break;
		case 'D':
			newY = y - distance;
			break;
		case 'R':
			newX = x + distance;
			break;
		case 'L':
			newX = x - distance;
			break;
		}

		return new Result(createLine(x, y, newX, newY), new Point(newX, newY));
	}

	private List<Point> createLine(int x, int y, int newX, int newY) {
		List<Point> line = new ArrayList<>();

		int lowX = Math.min(x, newX);
		int highX = Math.max(x, newX);
		int lowY = Math.min(y, newY);
		int highY = Math.max(y, newY);

		for (int i = lowX; i <= highX; i++) {
			for (int j = lowY; j <= highY; j++) {
				if (i != x || j != y) {
					line.add(new Point(i, j));
				}
			}
		}
		
		Collections.sort(line, (Point p1, Point p2) -> sortCorrectOrder(p1,p2,x,y));
		return line;
	}
	
	private int sortCorrectOrder(Point p1, Point p2, int x, int y) {
	
		return distance(p1, x, y) - distance (p2, x, y);	
	}

	private int distance(Point p1, int x, int y) {
		int calcX = Math.abs(x - p1.getX());
		int calcY = Math.abs(y - p1.getY());
		return calcX + calcY;
	}

	public static void main(String[] args) throws IOException {
		Day3 day3 = new Day3();
		day3.run();
	}

	private static final class Point {
		private final int x;
		private final int y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;

		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}
	}

	private static final class Result {
		private final Point newPoint;
		private final List<Point> line;

		public Result(List<Point> line, Point newPoint) {
			this.line = line;
			this.newPoint = newPoint;
		}
		
		Point getNewPoint() {
			return newPoint;
		}
		
		List<Point> getLine() {
			return line;
		}

	}
	
	private static final class MatchingPoints {
		private final Point pointA;
		private final Point pointB;
		
		public MatchingPoints(Point pointA, Point pointB) {
			this.pointA = pointA;
			this.pointB = pointB;
		}
		
		Point getPointA() {
			return pointA;
		}
		
		Point getPointB() {
			return pointB;
		}
	}

}