package days2019;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public final class Intprocessor {
	
	private final List<Integer> integers;
	
	public Intprocessor(List<Integer> integers) {
		this.integers = copyList(integers);
	}
	
	public List<Integer> getResult() {
		return copyList(integers);
	}
	
	private List<Integer> copyList(List<Integer> integers) {
		return integers.stream().collect(Collectors.toList());
	}
	
	public int run() {
		boolean finished = false;
		int index = 0; //Instruction pointer
		
		while(!finished) {
			int value = integers.get(index);
			Instructions instructions = splitInstructions(value);
			int instructionsValue = 1;
			switch (instructions.getOpCode()) {
			case 1:
				opCode1(instructions.getInputModes(), integers.get(index + 1), integers.get(index + 2), integers.get(index + 3));
				instructionsValue = 4;
				break;
			case 2:
				opCode2(instructions.getInputModes(), integers.get(index + 1), integers.get(index + 2), integers.get(index + 3));
				instructionsValue = 4;
				break;
			case 3:
				opCode3(integers.get(index+1));
				instructionsValue = 2;
				break;
			case 4:
				opCode4(integers.get(index+1));
				instructionsValue = 2;
				break;
			case 99:
				finished = true;
				break;
			default:
				finished = true;
				break;
			}
			
			index = index + instructionsValue;
		}
		
		// Diagnostic code
		return integers.get(0);
		
	}

	private void opCode1(int[] inputModes, int address1, int address2, int storeAddress) {
		// add the 2 numbers
		int sum = getInput(inputModes[0], address1) + getInput(inputModes[1], address2);
		integers.set(storeAddress, sum);		
	}
	
	private void opCode2(int[] inputModes, int address1, int address2, int storeAddress) {
		// multiply the 2 numbers
		int sum = getInput(inputModes[0], address1) * getInput(inputModes[1], address2);
		integers.set(storeAddress, sum);		
	}
	
    private void opCode3(Integer address) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input: ");
            int input = scanner.nextInt();
            integers.set(address, input);
        }
    }

	private void opCode4(Integer address) {
		System.out.println("code: " +  integers.get(address));
		
	}
	
	private int getInput(int inputMode, int input) {
		switch (inputMode) {
		case 0:
			return integers.get(input);
		case 1:
			return input;
		default:
			throw new IllegalArgumentException("unknown input mode: " + inputMode);
		}
	}

	public void printList() {
		for (Integer integer : integers) {
			System.out.print(integer + ",");
			
		}
		
		System.out.println("End");
		
	}
	
	private Instructions splitInstructions(int value) {
		int opCode = value % 100;
		
		value = value / 100;
		
		List<Integer> list = new ArrayList<>();
		
		while (value > 0) {
			list.add(value % 10);
			value = value / 10;
		}
		return new Instructions(opCode, list.stream().mapToInt(i -> i).toArray());
	}

	
	private class Instructions {
		int opCode;
		int[] inputModes;
		
		public Instructions(int opCode, int[] inputModes) {
			this.opCode = opCode;
			this.inputModes = getInPutModes(inputModes);
		}
		
		private int[] getInPutModes(int[] inputModes) {
			int[] modes = new int[] { 0, 0, 0, 0, 0, 0 };

			for (int i = 0; i < inputModes.length; i++) {
				modes[i] = inputModes[i];
			}

			return modes;
		}

		int getOpCode() {
			return opCode;
		}
		
		int[] getInputModes() {
			return inputModes;
		}
		
	}
}