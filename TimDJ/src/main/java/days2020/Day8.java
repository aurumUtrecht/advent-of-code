package days2020;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import days2020.helpclasses.Accumulator;
import days2020.helpclasses.AccumulatorUtils;
import days2020.helpclasses.EOperation;
import utils.FileReaderHelper;

/**
 * Day 8 of Advent of Code 2020
 *
 */
public final class Day8 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day8");
        int anwser1 = findLoop(list);
        System.out.println(anwser1);
        int anwser2 = findLoopCorrection(list);
        System.out.println(anwser2);
    }

    private int findLoop(List<String> list) {
        List<Integer> indexList = new ArrayList<>();
        Accumulator acc = new Accumulator();
        while (true) {

            int index = acc.getIndex();
            if (indexList.contains(index)) {
                return acc.getAccumulator();
            }
            indexList.add(index);

            String operation = list.get(index);
            acc.runOperation(AccumulatorUtils.getOperation(operation), AccumulatorUtils.getValue(operation));
        }
    }

    private int findLoopCorrection(List<String> list) {
        int finalIndex = list.size() - 1;
        int correctionIndex = 0;

        while (true) {
            correctionIndex++; // Check index 0 is not jump or no operation
            if (AccumulatorUtils.getOperation(list.get(correctionIndex)) == EOperation.ACCUMULATOR) {
                continue;
            }

            Accumulator acc = new Accumulator();
            List<Integer> indexList = new ArrayList<>();
            boolean infinitLoop = false;
            while (!infinitLoop) {
                int index = acc.getIndex();

                if (index == finalIndex) {
                    return acc.getAccumulator();
                }
                if (indexList.contains(index)) {
                    infinitLoop = true;
                }
                indexList.add(index);
                String operation = list.get(index);
                EOperation operationEnum = AccumulatorUtils.getOperation(operation);
                if (index == correctionIndex) {
                    operationEnum = reverseoperation(operationEnum);
                }
                acc.runOperation(operationEnum, AccumulatorUtils.getValue(operation));
            }
        }
    }

    private EOperation reverseoperation(EOperation operationEnum) {
        return operationEnum == EOperation.JUMP ? EOperation.NO_OPERATION : EOperation.JUMP;
    }

    public static void main(String[] args) throws IOException {
        Day8 day = new Day8();
        day.run();
    }
}
