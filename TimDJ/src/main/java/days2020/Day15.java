package days2020;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Day 15 of Advent of Code 2020
 *
 */
public final class Day15 {
    private static final int[] testInput1 = new int[] { 0, 3, 6 };
    private static final int[] testInput2 = new int[] { 1, 3, 2 };
    private static final int[] testInput3 = new int[] { 2, 1, 3 };
    private static final int[] testInput4 = new int[] { 1, 2, 3 };
    private static final int[] testInput5 = new int[] { 2, 3, 1 };
    private static final int[] testInput6 = new int[] { 3, 2, 1 };
    private static final int[] testInput7 = new int[] { 3, 1, 2 };
    private static final int[] input = new int[] { 0, 14, 6, 20, 1, 4 };

    private void run()  {
        runTestCode();
        System.out.println(findNr(input, 30000000));
    }

    private void runTestCode() {
        int expectedAnswer1 = 436;
        int expectedAnswer2 = 1;
        int expectedAnswer3 = 10;
        int expectedAnswer4 = 27;
        int expectedAnswer5 = 78;
        int expectedAnswer6 = 438;
        int expectedAnswer7 = 1836;

        int result1 = findNr(testInput1, 30000000);
        int result2 = findNr(testInput2, 30000000);
        int result3 = findNr(testInput3, 30000000);
        int result4 = findNr(testInput4, 30000000);
        int result5 = findNr(testInput5, 30000000);
        int result6 = findNr(testInput6, 30000000);
        int result7 = findNr(testInput7, 30000000);

        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer1, result1,
                expectedAnswer1 == result1));
        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer2, result2,
                expectedAnswer2 == result2));
        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer3, result3,
                expectedAnswer3 == result3));
        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer4, result4,
                expectedAnswer4 == result4));
        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer5, result5,
                expectedAnswer5 == result5));
        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer6, result6,
                expectedAnswer6 == result6));
        System.out.println(String.format("Expected: %4d, was: %4d, are equal: %s", expectedAnswer7, result7,
                expectedAnswer7 == result7));

    }

    private int findNr(int[] input, int nr) {
        Map<Integer, StoredNumbers> numbers = new HashMap<>();

        int lastNr = 0;
        for (int i = 1; i <= input.length; i++) {
            StoredNumbers nmbr = numbers.get(input[i - 1]) ;
             
            if (nmbr == null) { 
            numbers.put(input[i - 1], new StoredNumbers(i));
            lastNr = 0;
            } else {
                lastNr = 1;
                nmbr.addLast(i);
            }
        }

        for (int i = input.length + 1; i < nr; i++) {
            StoredNumbers nmbr = numbers.get(lastNr);
            if (nmbr == null) {
                numbers.put(lastNr, new StoredNumbers(i));
                lastNr = 0;
            } else {
                nmbr.addLast(i);
                lastNr = nmbr.getDifference();
            }
        }
        return lastNr;
    }

    public static void main(String[] args) throws IOException {
        Day15 day = new Day15();
        day.run();
    }

    private final class StoredNumbers {
        private int last;
        private int secondToLast;

        public StoredNumbers(int last) {
            this.last = last;
            this.secondToLast = 0;
        }

        void addLast(int newLast) {
            this.secondToLast = this.last;
            this.last = newLast;
        }

        int getDifference() {
            return this.last - this.secondToLast;
        }
    }

}
