package days2020;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 2 of Advent of Code 2020
 *
 */
public final class Day2 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2020/day2");
		int validPasswords = findValidPasswords(list, 1);
		System.out.println(validPasswords);

		int validPasswords2 = findValidPasswords(list, 2);
		System.out.println(validPasswords2);
	}

	private int findValidPasswords(List<String> list, int version) {
		int amount = 0;
		for (String password : list) {
			if (version == 1 && passwordValid1(password)) {
				amount++;
			}
			if (version == 2 && passwordValid2(password)) {
				amount++;
			}
		}
		return amount;
	}

	private boolean passwordValid1(String passwordString) {
		int minAmount = getFirstIndex(passwordString);
		int maxAmount = getSecondIndex(passwordString);
		char letter = getletter(passwordString);
		String password = getPassword(passwordString);

		long charCount = password.chars().filter(p -> p == letter).count();

		return charCount >= minAmount && charCount <= maxAmount;
	}

	private boolean passwordValid2(String passwordString) {
		int firstIndex = getFirstIndex(passwordString);
		int secondIndex = getSecondIndex(passwordString);
		char letter = getletter(passwordString);
		String password = getPassword(passwordString);

		char letter1 = password.charAt(firstIndex - 1);
		char letter2 = password.charAt(secondIndex - 1);

		return (letter1 == letter && letter2 != letter) || (letter1 != letter && letter2 == letter);
	}

	private int getFirstIndex(String passwordString) {
		return Integer.parseInt(passwordString.split("-")[0]);
	}

	private int getSecondIndex(String passwordString) {
		int start = passwordString.indexOf('-');
		int end = passwordString.indexOf(" ");

		return Integer.parseInt(passwordString.substring(start + 1, end));
	}

	private char getletter(String passwordString) {
		int index = passwordString.indexOf(':');
		return passwordString.charAt(index - 1);
	}

	private String getPassword(String passwordString) {
		return passwordString.split(" ")[2];
	}

	public static void main(String[] args) throws IOException {
		Day2 day1 = new Day2();
		day1.run();
	}
}
