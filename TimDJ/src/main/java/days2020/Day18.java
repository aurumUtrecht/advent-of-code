package days2020;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 18 of Advent of Code 2020
 *
 */
public final class Day18 {

	private void run() throws IOException {
	    List<String> list = FileReaderHelper.readFile("input2020/day18");
		
	}


	public static void main(String[] args) throws IOException {
		Day18 day = new Day18();
		day.run();
	}
}
