package days2020.helpclasses;

public enum EGeoObject {
	OPEN_SQUARE('.'), 
	TREE('#');

	private final char sign;

	private EGeoObject(char sign) {
		this.sign = sign;
	}

	public char getSign() {
		return sign;
	}

	public static EGeoObject getGeoObject(char c) {
		for (EGeoObject geoObject : EGeoObject.values()) {
			if (geoObject.getSign() == c) {
				return geoObject;
			}
		}
		throw new IllegalArgumentException("unknown sign " + c);
	}
}
