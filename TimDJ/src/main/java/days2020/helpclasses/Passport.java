/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020.helpclasses;

/**
 * Model for passport
 */
public final class Passport {
    
    private final int byr;
    
    private final int iyr;
    
    private final int eyr;
    
    private final int hgt;
    
    private final String hcl;
    
    private final String ecl;
    
    private final String pid;
    
    private final String cid;
    
    /**
     * Constructor
     */
    public Passport(int birthYear, int issueYear, int expirationYear, int height, String hairColor, String eyeColor,
            String passportId, String countryId) {
        super();
        this.byr = birthYear;
        this.iyr = issueYear;
        this.eyr = expirationYear;
        this.hgt = height;
        this.hcl = hairColor;
        this.ecl = eyeColor;
        this.pid = passportId;
        this.cid = countryId;
    }

    public int getBirthYear() {
        return byr;
    }

    public int getIssueYear() {
        return iyr;
    }

    public int getExpirationYear() {
        return eyr;
    }

    public int getHeight() {
        return hgt;
    }

    public String getHairColor() {
        return hcl;
    }

    public String getEyeColor() {
        return ecl;
    }

    public String getPassportId() {
        return pid;
    }

    public String getCountryId() {
        return cid;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Birth Year: ");
        sb.append(byr);
        sb.append(" Issue Year: ");
        sb.append(iyr);
        sb.append(" Expiration Year: ");
        sb.append(eyr);
        sb.append(" Height: ");
        sb.append(hgt);
        sb.append(" Hair Colorr: ");
        sb.append(hcl);
        sb.append(" Eye Color: ");
        sb.append(ecl);
        sb.append(" PassportID: ");
        sb.append(pid);
        sb.append(" CountryID: ");
        sb.append(cid);
        return sb.toString();
    }
}