package days2020.helpclasses;

import java.util.ArrayList;
import java.util.List;

public final class ToboggganMapUtils {

	private ToboggganMapUtils() {
		// Utility class
	}

	public static List<EGeoObject> getAllObjects(List<String> map, int right, int down) {
		List<EGeoObject> result = new ArrayList<>();

		int index = 0;
		int lineIndex = 0;

		while (lineIndex < map.size()) {
			String line = map.get(lineIndex);
			result.add(getGeoObject(line, index));
			index = index + right;
			lineIndex = lineIndex + down;
		}

		return result;
	}

	public static EGeoObject getGeoObject(String line, int position) {
		int maxLength = line.length();
		int index = position;
		if (position >= maxLength) {
			index = findIndex(position, maxLength);
		}
		return EGeoObject.getGeoObject(line.charAt(index));
	}

	private static int findIndex(int position, int maxLength) {
		if (position < maxLength) {
			return position;
		}
		return findIndex((position - maxLength), maxLength);

	}
}
