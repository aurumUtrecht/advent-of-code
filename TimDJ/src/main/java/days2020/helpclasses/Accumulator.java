/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020.helpclasses;

/**
 * Accumulator for day 8
 */
public final class Accumulator {

    private int accumulator;
    private int index;
    
    /**
     * Constructor
     */
    public Accumulator() {
        this.accumulator = 0;
        this.index = 0;
    }

    public int getAccumulator() {
        return accumulator;
    }

    public int getIndex() {
        return index;
    }
    
    public void runOperation(EOperation operation, int value) {
        switch (operation) {
        case ACCUMULATOR:
            accumulator += value;
            index += 1;
            break;
        case JUMP:
            index += value;
            break;
        case NO_OPERATION:
            index += 1;
            break;
        default:
            throw new IllegalArgumentException();
        }
    }
}
