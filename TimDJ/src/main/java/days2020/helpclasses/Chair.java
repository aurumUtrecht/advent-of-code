/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020.helpclasses;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for the chair
 */
public class Chair {

    private final int positionX;
    private final int positionY;
    private final List<Chair> adjecentChairs = new ArrayList<>();

    private boolean occupied;
    private boolean newState;

    public Chair(int positionY, int positionX) {
        this.positionY = positionY;
        this.positionX = positionX;
        occupied = false;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public List<Chair> getAdjecentChairs() {
        return adjecentChairs;
    }

    public boolean isOccupied() {
        return occupied;
    }
    
    public void setNewState(boolean newState) {
        this.newState = newState;
    }
    
    public void commit() {
        this.occupied = this.newState;
    }

    public void addAdjecentChair(Chair chair) {
        if ( chair == null) {
            System.out.println(this);
            throw new RuntimeException();
        }
        adjecentChairs.add(chair);
    }

    public int countAdjecentOccupied() {
        return (int)adjecentChairs.stream().filter(i -> i.isOccupied()).count();
    }
    
    @Override
    public String toString() {
        return String.format("Chair at %d, %d, occupied = %s", positionY, positionX, occupied);
    }
}
