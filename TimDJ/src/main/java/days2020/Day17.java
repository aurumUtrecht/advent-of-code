package days2020;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 17 of Advent of Code 2020
 *
 */
public final class Day17 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2020/day17");
	
	}

	

	public static void main(String[] args) throws IOException {
		Day17 day = new Day17();
		day.run();
	}
}
