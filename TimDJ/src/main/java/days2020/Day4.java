package days2020;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 4 of Advent of Code 2020
 *
 */
public final class Day4 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day4");
        List<String> passportList = createPassportStrings(list);

        int validPassports1 = validatePassports1(passportList);
        System.out.println(validPassports1);

        int validPassports2 = validatePassports2(passportList);
        System.out.println(validPassports2);
    }

    private List<String> createPassportStrings(List<String> list) {
        List<String> passportList = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        for (String line : list) {
            if (!line.isEmpty()) {
                sb.append(line);
                sb.append(" ");
            } else {
                passportList.add(sb.toString());
                sb = new StringBuilder();
            }
        }

        passportList.add(sb.toString());
        return passportList;
    }

    private int validatePassports1(List<String> passportList) {
        int validpassports = 0;

        for (String passport : passportList) {
            if (passport.contains("byr") && passport.contains("iyr") && passport.contains("eyr") &&
                    passport.contains("hgt") && passport.contains("hcl") && passport.contains("ecl") &&
                    passport.contains("pid")) {
                validpassports++;
            }
        }

        return validpassports;
    }

    private int validatePassports2(List<String> passportList) {
        int validpassports = 0;
        for (String passport : passportList) {
            if (passport.contains("byr") && passport.contains("iyr") && passport.contains("eyr") &&
                    passport.contains("hgt") && passport.contains("hcl") && passport.contains("ecl") &&
                    passport.contains("pid")) {
                if (validatePassportParts(passport)) {
                    validpassports++;
                }
            }
        }
        return validpassports;
    }

    private boolean validatePassportParts(String passport) {
        String[] parts = passport.split(" ");

        for (String part : parts) {
            String[] value = part.split(":");
            switch (value[0]) {
            case "byr":
                int byear = Integer.parseInt(value[1]);
                if (byear < 1920 || byear > 2002) {
                    return false;
                }
                break;
            case "iyr":
                int iyear = Integer.parseInt(value[1]);
                if (iyear < 2010 || iyear > 2020) {
                    return false;
                }
                break;
            case "eyr":
                int eyear = Integer.parseInt(value[1]);
                if (eyear < 2020 || eyear > 2030) {
                    return false;
                }
                break;
            case "hgt":
                int height = Integer.parseInt(value[1].replaceAll("[^\\d.]", ""));
                if (value[1].contains("cm")) {
                    if (height < 150 || height > 193) {
                        return false;
                    }
                } else if (value[1].contains("in")) {
                    if (height < 59 || height > 76) {
                        return false;
                    }
                } else {
                    return false;
                }
                break;
            case "hcl":
                String hair = value[1];
                if (hair.charAt(0) != '#' || hair.length() != 7) {
                    return false;
                }
                break;
            case "ecl":
                String eyes = value[1];
                if (!(eyes.equals("amb") || eyes.equals("blu") || eyes.equals("brn") || eyes.equals("gry") ||
                        eyes.equals("grn") || eyes.equals("hzl") || eyes.equals("oth"))) {
                    return false;
                }
                break;
            case "pid":
                String passportId = value[1];
                if (passportId.length() != 9) {
                    return false;
                }
                break;
            case "cid":
                // Ignored
                break;
            }
        }
        return true;
    }

    public static void main(String[] args) throws IOException {
        Day4 day = new Day4();
        day.run();
    }
}
