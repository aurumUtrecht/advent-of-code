package days2020;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import utils.FileReaderHelper;

/**
 * Day 13 of Advent of Code 2020
 *
 */
public final class Day13 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day13");
        int timeStamp = Integer.parseInt(list.get(0));
        Map<Integer, Integer> busNumbers = getAllNumber(list.get(1));

//        int anwser = findAnwser(busNumbers, timeStamp);
//        System.out.println(anwser);

//        long anwser2 = findAnwser2(busNumbers);
//        System.out.println(anwser2);
        testData();

    }

    private void testData() {
        Map<Integer, Integer> testData1 = new HashMap<>();
        testData1.put(7, 0);
        testData1.put(13, 1);
        testData1.put(59, 4);
        testData1.put(31, 6);
        testData1.put(19, 7);
        
        Map<Integer, Integer> testData2 = new HashMap<>();
        testData2.put(17, 0);
        testData2.put(13, 2);
        testData2.put(19, 3);

        Map<Integer, Integer> testData3 = new HashMap<>();
        testData3.put(67, 0);
        testData3.put(7, 1);
        testData3.put(59, 2);
        testData3.put(61, 3);
        
        Map<Integer, Integer> testData4 = new HashMap<>();
        testData4.put(67, 0);
        testData4.put(7, 2);
        testData4.put(59, 3);
        testData4.put(61, 4);
        
        Map<Integer, Integer> testData5 = new HashMap<>();
        testData5.put(67, 0);
        testData5.put(7, 1);
        testData5.put(59, 3);
        testData5.put(61, 4);
        
        Map<Integer, Integer> testData6 = new HashMap<>();
        testData6.put(1789, 0);
        testData6.put(37, 1);
        testData6.put(47, 2);
        testData6.put(1889, 3);
        
        long expected1 = 1068788;
        long expected2 = 3417;
        long expected3 = 754018;
        long expected4 = 779210;
        long expected5 = 1261476;
        long expected6 = 1202161486;
        
        long result1 = findAnwser2(testData1);
        long result2 = findAnwser2(testData2);
        long result3 = findAnwser2(testData3);
        long result4 = findAnwser2(testData4);
        long result5 = findAnwser2(testData5);
        long result6 = findAnwser2(testData6);
        
        System.out.println(String.format("Expected %d, was %d, are equal: %s", expected1, result1, expected1 == result1));
        System.out.println(String.format("Expected %d, was %d, are equal: %s", expected2, result2, expected2 == result2));
        System.out.println(String.format("Expected %d, was %d, are equal: %s", expected3, result3, expected3 == result3));
        System.out.println(String.format("Expected %d, was %d, are equal: %s", expected4, result4, expected4 == result4));
        System.out.println(String.format("Expected %d, was %d, are equal: %s", expected5, result5, expected5 == result5));
        System.out.println(String.format("Expected %d, was %d, are equal: %s", expected6, result6, expected6 == result6));
        
    }

    private int findAnwser(Map<Integer, Integer> busNumbers, int timeStamp) {
        int shortestWaitingTime = Integer.MAX_VALUE;
        int busNr = 0;
        for (int nr : busNumbers.keySet()) {
            int waitingTime = nr - (timeStamp % nr);
            if (waitingTime < shortestWaitingTime) {
                shortestWaitingTime = waitingTime;
                busNr = nr;
            }
        }

        return busNr * shortestWaitingTime;
    }

    private long findAnwser2(Map<Integer, Integer> busNumbers) {

        long timeStamp = 1;

        for (Entry<Integer, Integer> bus : busNumbers.entrySet()) {
            timeStamp = findSingleTimeStamp(bus) * timeStamp;
            if (timeStamp == 0) {
                timeStamp = 1;
            }
        }

        return findTimeStamp(busNumbers, timeStamp);
    }
    
    private long findSingleTimeStamp(Entry<Integer, Integer> bus) {
        return findTimeStamp(Map.of(bus.getKey(), bus.getValue()), 1);
    }

    private long findTimeStamp(Map<Integer, Integer> busNumbers, long interval) {
        System.out.println(" interval " + interval);

        long timeStamp = 0-interval;
        boolean timeStampFound = false;
        int runs = 0;

        while (!timeStampFound) {
            runs++;
            if (runs % 10000000000000L == 0) {
                System.out.println(timeStamp);
            }
            timeStamp += interval;
            timeStampFound = true;

            for (Entry<Integer, Integer> bus : busNumbers.entrySet()) {
                if (!((bus.getValue() + timeStamp) % bus.getKey() == 0)) {
                    // invalid timeStamp
                    timeStampFound = false;
                    break;
                }
            }

        }
        return timeStamp;
    }

    private Map<Integer, Integer> getAllNumber(String busLines) {
        String[] split = busLines.split(",");
        Map<Integer, Integer> busNumbers = new LinkedHashMap<>();
        int index = 0;

        for (String bus : split) {
            if (bus.equals("x")) {
                index++;
                continue;
            }
            busNumbers.put(Integer.parseInt(bus), index);
            index++;
        }
        return busNumbers;
    }

    public static void main(String[] args) throws IOException {
        Day13 day = new Day13();
        day.run();
    }
}
