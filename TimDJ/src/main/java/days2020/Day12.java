package days2020;

import java.io.IOException;
import java.util.List;

import utils.CalculationHelper;
import utils.ECompassDirection;
import utils.ETurn;
import utils.FileReaderHelper;

/**
 * Day 12 of Advent of Code 2020
 *
 */
public final class Day12 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day12");

        int manhattanDistance = calculateDistance(list);
        System.out.println(manhattanDistance);

        int manhattanDistance2 = calculateDistance2(list);
        System.out.println(manhattanDistance2);
    }

    private int calculateDistance(List<String> list) {
        int currentX = 0;
        int currentY = 0;
        ECompassDirection currentDirection = ECompassDirection.EAST;

        for (String instruction : list) {
            char action = instruction.charAt(0);
            int value = Integer.parseInt(instruction.substring(1));

            switch (action) {
            case 'N':
                currentY += value;
                break;
            case 'S':
                currentY -= value;
                break;
            case 'E':
                currentX += value;
                break;
            case 'W':
                currentX -= value;
                break;
            case 'L':
                currentDirection = ECompassDirection.getNewDirection(currentDirection, ETurn.LEFT, value);
                break;
            case 'R':
                currentDirection = ECompassDirection.getNewDirection(currentDirection, ETurn.RIGHT, value);
                break;
            case 'F':
                if (currentDirection == ECompassDirection.NORTH) {
                    currentY += value;
                }
                if (currentDirection == ECompassDirection.SOUTH) {
                    currentY -= value;
                }
                if (currentDirection == ECompassDirection.EAST) {
                    currentX += value;
                }
                if (currentDirection == ECompassDirection.WEST) {
                    currentX -= value;
                }
                break;
            }
        }
        return CalculationHelper.caluculateManhattanDistance(currentX, currentY);
    }

    private int calculateDistance2(List<String> list) {
        int currentY = 0;
        int currentX = 0;
        int wayPointY = 1;
        int wayPointX = 10;

        for (String instruction : list) {
            char action = instruction.charAt(0);
            int value = Integer.parseInt(instruction.substring(1));

            switch (action) {
            case 'N':
                wayPointY += value;
                break;
            case 'S':
                wayPointY -= value;
                break;
            case 'E':
                wayPointX += value;
                break;
            case 'W':
                wayPointX -= value;
                break;
            case 'L':
                int newY = findNewY(wayPointY, wayPointX, ETurn.LEFT, value);
                wayPointX = findNewX(wayPointY, wayPointX, ETurn.LEFT, value);
                wayPointY = newY;
                break;
            case 'R':
                int newY2 = findNewY(wayPointY, wayPointX, ETurn.RIGHT, value);
                wayPointX = findNewX(wayPointY, wayPointX, ETurn.RIGHT, value);
                wayPointY = newY2;
                break;
            case 'F':
                currentY += (wayPointY * value);
                currentX += (wayPointX * value);
                break;
            }
        }
        return CalculationHelper.caluculateManhattanDistance(currentX, currentY);
    }

    private int findNewY(int wayPointY, int wayPointX, ETurn turnDirection, int value) {
        int turnRights = ETurn.findTurnRights(turnDirection, value / 90);
        return findNewValue(wayPointY, wayPointX, turnRights);
    }

    private int findNewX(int wayPointY, int wayPointX, ETurn turnDirection, int value) {
        int turnLefts = ETurn.findTurnLefts(turnDirection, value / 90);
        return findNewValue(wayPointX, wayPointY, turnLefts);
    }

    private int findNewValue(int value1, int value2, int turns) {
        if (turns == 1) {
            return -value2;
        }
        if (turns == 2) {
            return -value1;
        }
        if (turns == 3) {
            return value2;
        }
        return 0;
    }

    public static void main(String[] args) throws IOException {
        Day12 day = new Day12();
        day.run();
    }
}
