package days2020;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import utils.FileReaderHelper;

/**
 * Day 6 of Advent of Code 2020
 *
 */
public final class Day6 {

	private void run() throws IOException {
		String file = FileReaderHelper.readFileSingelString("input2020/day6");
		
        long amount1 = Arrays.stream(file.split("\n\r"))
                .map(s -> s.replace("\n", "").replace("\r", ""))
                .map(p -> p.chars().distinct().count())
                .reduce(0L, Long::sum);
		
		System.out.println(amount1);
		
        long amount2 = Arrays.stream(file.split("\n\r"))
                .mapToInt(s -> countAnwsers(s))
                .sum();
	        
	        System.out.println(amount2);
	}

    private int countAnwsers(String anwsers) {
        String[] seperated = anwsers.split("\r");

        if (seperated.length == 1) {
            return seperated[0].replace("\n", "").toCharArray().length;
        }

        List<Character> allChars = seperated[0].replace("\n", "").replace("\r", "")
                .chars()
                .mapToObj(c -> (char)c)
                .collect(Collectors.toList());
        
        for (int i = 1; i < seperated.length; i++) {
            String currentString = seperated[i];
            allChars = allChars.stream().filter(c -> currentString.contains("" + c)).collect(Collectors.toList());
        }
        return allChars.size();
    }
    
    public static void main(String[] args) throws IOException {
		Day6 day = new Day6();
		day.run();
	}
}
