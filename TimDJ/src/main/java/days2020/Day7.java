package days2020;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import days2020.helpclasses.Bag;
import utils.FileReaderHelper;

/**
 * Day 7 of Advent of Code 2020
 *
 */
public final class Day7 {
    private final Map<String, Bag> allBags = new HashMap<>();

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day7");

        for (String rule : list) {
            String name = getName(rule);
            String[] children = getChildren(rule);
            addBag(name, children);
        }

        long amount1 = cointainsShinyGold();
        System.out.println(amount1);

        long amount2 = insideShinyGold();
        System.out.println(amount2);
    }

    private long cointainsShinyGold() {
        Bag shineyGold = getBag("shiny gold");
        return shineyGold.getAllParent().stream().distinct().count();
    }

    private long insideShinyGold() {
        Bag shineyGold = getBag("shiny gold");
        return countChildren(shineyGold.getChildren());
    }

    private long countChildren(Map<Bag, Integer> children) {
        int amount = 0;
        for (Map.Entry<Bag, Integer> entry : children.entrySet()) {
            System.out.println(entry.getKey().getName());
            amount += entry.getValue();
            amount += (countChildren(entry.getKey().getChildren()) * entry.getValue());
        }
        return amount;
    }

    private String[] getChildren(String rule) {
        return rule.split("contain ")[1].split(",");
    }

    private String getName(String rule) {
        return rule.split("bags")[0];
    }

    private void addBag(String bagName, String... children) {
        Bag bag = getBag(bagName);
        if (children[0].contains("no other bags.")) {
            return;
        }
        for (String childString : children) {
            String correctedString = childString.stripLeading();
            int amount = Integer.parseInt(correctedString.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[0]);
            String color = correctedString.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[1].stripLeading()
                    .replace(" bags", "");
            Bag child = getBag(color);
            bag.addChild(child, amount);
            child.addParent(bag);
        }
    }

    private Bag getBag(String color) {
        String correctedColor = color.replace(".", "").replace("bag", "").stripLeading().stripTrailing();
        Bag bag = allBags.get(correctedColor);
        if (bag == null) {
            bag = new Bag(correctedColor);
            allBags.put(correctedColor, bag);
        }
        return bag;
    }

    public static void main(String[] args) throws IOException {
        Day7 day = new Day7();
        day.run();
    }
}
