package days2020;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import utils.FileReaderHelper;

/**
 * Day 10 of Advent of Code 2020
 *
 */
public final class Day10 {

    private void run() throws IOException {
        List<Integer> list = FileReaderHelper.readFileToInt("input2020/day10");
        List<Integer> sortedList = list.stream().sorted().collect(Collectors.toList());

        // Add first and last adapter
        sortedList.add(0, 0);
        sortedList.add((sortedList.get(sortedList.size() - 1) + 3));

        int result = findResult(sortedList);
        System.out.println(result);

        long result2 = findResult2(sortedList);
        System.out.println(result2);
    }

    public int findResult(List<Integer> sortedList) {
        int oneSteps = 0;
        int threeSteps = 0;
        for (int i = 1; i < sortedList.size(); i++) {
            int difference = sortedList.get(i) - sortedList.get(i - 1);
            if (difference % 3 == 0) {
                threeSteps++;
            } else {
                oneSteps++;
            }
        }
        return oneSteps * threeSteps;
    }

    public long findResult2(List<Integer> sortedList) {
        List<List<Integer>> splittedList = splitList(sortedList);
        List<Integer> possabilities = new ArrayList<>();
        
        for (List<Integer> partialList : splittedList) {
            possabilities.add(findAllPosabilitys(partialList));
        }

        long totalvalue = 1;
        for (int value : possabilities) {
            totalvalue = totalvalue * value;
        }

        return totalvalue;
    }

    private int findAllPosabilitys(List<Integer> partialList) {
        if(partialList.size() == 1) {
            return 1;
        }
        
        if(partialList.size() == 2) {
            return 1;
        }
        
        if(partialList.size() == 3) {
            return 2;
        }
        
        if(partialList.size() == 4) {
            return 4;
        }
        
        if(partialList.size() == 5) {
            return 7;
        }
        return 0;
    }

    private List<List<Integer>> splitList(List<Integer> sortedList) {
        List<List<Integer>> splitList = new ArrayList<>();
        // Split the list on 3-step places
        int indexLastSplit = 0;
        for (int i = 1; i < sortedList.size(); i++) {
            int difference = sortedList.get(i) - sortedList.get(i - 1);
            if (difference % 3 == 0) {
                splitList.add(sortedList.subList(indexLastSplit, i));
                indexLastSplit = i;
            } 
        }
        return splitList;
    }

    public static void main(String[] args) throws IOException {
        Day10 day = new Day10();
        day.run();
    }
}
