package days2020;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 9 of Advent of Code 2020
 *
 */
public final class Day9 {

    private void run() throws IOException {
        List<Long> list = FileReaderHelper.readFileToLong("input2020/day9");
        long anwser = finderrorIndex(list);
        System.out.println(anwser);
        long weakness = findWeakness(list, anwser);
        System.out.println(weakness);
    }

    private long finderrorIndex(List<Long> list) {
        for (int i = 25; i < list.size(); i++) {
            long currentNumber = list.get(i);
            boolean valid = false;
            for (int j = i - 25; j < i - 1; j++) {
                if (valid) {
                    break;
                }
                for (int k = j + 1; k < i; k++) {
                    if (list.get(j) + list.get(k) == currentNumber) {
                        valid = true;
                        break;
                    }
                }
            }
            if (!valid) {
                return currentNumber;
            }
        }
        return -1;
    }

    private long findWeakness(List<Long> list, long anwser) {
        for (int i = 0; i < list.size(); i++) {
            long total = 0;
            long lowest = Long.MAX_VALUE;
            long highest = Long.MIN_VALUE;
            for (int j = i; j < list.size(); j++) {
                long num = list.get(j);
                total += num;
                if (num < lowest) {
                    lowest = num;
                }
                if (num > highest) {
                    highest = num;
                }

                if (total > anwser) {
                    break;
                }
                if (total == anwser) {
                    return lowest + highest;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) throws IOException {
        Day9 day = new Day9();
        day.run();
    }
}
