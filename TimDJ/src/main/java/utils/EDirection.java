package utils;

public enum EDirection {
	FORWARD ("forward", 'F'),
	DOWN ("down", 'D'),
	UP ("up", 'U'),
	LEFT("left", 'L'),
	RIGHT("right", 'R');
	

	private final String directionName;
	private final char directionLetter;
	
	private EDirection(String directionName, char directionLetter) {
		this.directionName = directionName;
		this.directionLetter = directionLetter;
	}
	
	public String getDirectionName() {
		return directionName;
	}
	
	public char getDirectionLetter() {
		return directionLetter;
	}
	
	public static EDirection getDirection(String value) {
		for (EDirection direction : EDirection.values()) {
			if (direction.getDirectionName().equals(value)) {
				return direction;
			}
		}
		throw new IllegalArgumentException();
	}
	
	public static EDirection getDirection(char value) {
		for (EDirection direction : EDirection.values()) {
			if (direction.getDirectionLetter() == value) {
				return direction;
			}
		}
		throw new IllegalArgumentException();
	}

}
