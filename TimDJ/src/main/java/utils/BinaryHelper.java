package utils;

/**
 * Helper class for Binary functions
 */
public class BinaryHelper {
	
	private BinaryHelper() {
		// Utility class
	}
	
	public static int binaryToNumber(String binary) {
		return Integer.parseInt(binary, 2);
	}
	
	public static String numberToBinary(int number) {
		return Integer.toBinaryString(number);
	}

}
