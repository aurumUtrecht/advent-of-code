package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class FileReaderHelper {

	public static final Pattern SPLIT_COMMA = Pattern.compile("\\s*,\\s*");

	private FileReaderHelper() {
		// Utility class
	}

	public static List<Integer> readFileToInt(String fileName) throws IOException {
		try (InputStream inputStream = FileReaderHelper.class.getClassLoader().getResourceAsStream((fileName))) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			return reader.lines().flatMap(SPLIT_COMMA::splitAsStream).map(i -> Integer.parseInt(i))
					.collect(Collectors.toList());
		}
	}

	public static List<Long> readFileToLong(String fileName) throws IOException {
		try (InputStream inputStream = FileReaderHelper.class.getClassLoader().getResourceAsStream((fileName))) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			return reader.lines().flatMap(SPLIT_COMMA::splitAsStream).map(i -> Long.parseLong(i))
					.collect(Collectors.toList());
		}
	}

	public static List<String> readFile(String fileName) throws IOException {
		try (InputStream inputStream = FileReaderHelper.class.getClassLoader().getResourceAsStream((fileName))) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			return reader.lines().collect(Collectors.toList());
		}
	}
	
	public static String readFileSingelString(String fileName) throws IOException {
		return Files.readString(new File("src/main/resources/" + fileName).toPath());
	}
	
	public static int[][] readFilesToIntMatrix(String fileName) throws IOException {
		return readFile(fileName).stream()
				.map(s -> Arrays.stream(s.split("|")).mapToInt(i -> Integer.parseInt(i)).toArray())
				.toArray(int[][]::new);
	}
	
	public static char[][] readFilesToCharMatrix(String fileName) throws IOException {
		return readFile(fileName).stream().map(s -> s.toCharArray()).toArray(char[][]::new);
	}
}