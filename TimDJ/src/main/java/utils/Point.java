package utils;

import java.util.Objects;

/**
 * Simple point class
 */
public final class Point {

	private final int x;
	private final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point(String x, String y) {
		this(Integer.parseInt(x), Integer.parseInt(y));
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getManhattanDistance(Point other) {
		return CalculationHelper.caluculateManhattanDistance(other.getY() - y, other.getX() - x);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x * 5 , y * 7);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (this.getClass() != o.getClass())
			return false;
		Point point = (Point) o;
		return x == point.getX() && y == point.getY();
	}
}
