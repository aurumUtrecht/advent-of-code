/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package utils;

/**
 * Turning enum
 */
public enum ETurn {
    LEFT,
    RIGHT;

    public static int findTurnLefts(ETurn direction, int value) {
        int reducedValue = value < 4 ? value : value % 4;

        if (direction == ETurn.LEFT) {
            return reducedValue;
        }

        return reverseTurns(reducedValue);
    }

    public static int findTurnRights(ETurn direction, int value) {
        int reducedValue = value < 4 ? value : value % 4;

        if (direction == ETurn.RIGHT) {
            return reducedValue;
        }
        return reverseTurns(reducedValue);
    }

    private static int reverseTurns(int value) {
        if (value == 1) {
            return 3;
        }

        if (value == 2) {
            return 2;
        }

        if (value == 3) {
            return 1;
        }
        return 0;
    }
}
