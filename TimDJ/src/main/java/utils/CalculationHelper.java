/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package utils;

/**
 * Utillity for common calculation problems
 */
public class CalculationHelper {

	private CalculationHelper() {
		// Utility class
	}

	public static int caluculateManhattanDistance(int y, int x) {
		return Math.abs(y) + Math.abs(x);
	}

	public static int calculateDifference(int y, int x) {
		return y > x ? y - x : x - y;
	}
}
