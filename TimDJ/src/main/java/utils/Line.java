package utils;

/**
 * Line based on 2 {#Code: Point}
 *
 */
public final class Line {
	private final Point startPoint;
	private final Point endPoint;
	
	public Line(Point startPoint, Point endPoint) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}
	
	public Point getStartPoint() {
		return startPoint;
	}
	public Point getEndPoint() {
		return endPoint;
	}
}
