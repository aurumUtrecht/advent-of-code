package days2021;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import days2021.helpclasses.Bingo;
import utils.FileReaderHelper;

/**
 * Day 4 of Advent of Code 2021
 *
 */
public final class Day4 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2021/day4");
		List<Bingo> bingoCards = createBingoCards(list);
		int[] numbers = Arrays.stream(list.get(0).split(",")).mapToInt(i -> Integer.parseInt(i)).toArray();

		int anwser = method1(bingoCards, numbers);
		System.out.println(anwser);
		int anwser2 = method2(bingoCards, numbers);
		System.out.println(anwser2);
	}

	private int method1(List<Bingo> bingoCards, int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			int nr = numbers[i];
			for (Bingo bingo : bingoCards) {
				bingo.mark(nr);
				if (bingo.bingo()) {
					return bingo.countScore() * nr;
				}
			}
		}
		return -1;
	}

	private int method2(List<Bingo> bingoCards, int[] numbers) {
		List<Bingo> currentbingoList = bingoCards;
		for (int i = 0; i < numbers.length; i++) {
			int nr = numbers[i];

			for (Bingo bingo : bingoCards) {
				bingo.mark(nr);
			}

			currentbingoList = currentbingoList.stream().filter(b -> !b.bingo()).collect(Collectors.toList());

			if (currentbingoList.size() == 1) {
				Bingo finalCard = currentbingoList.get(0);
				for (int j = i; i < numbers.length; j++) {
					finalCard.mark(numbers[j]);
					if (finalCard.bingo()) {
						return finalCard.countScore() * numbers[j];
					}
				}
			}
		}
		return -1;
	}

	private List<Bingo> createBingoCards(List<String> list) {
		List<Bingo> bingoList = new ArrayList<>();
		for (int i = 2; i < list.size(); i = i + 6) {
			bingoList.add(new Bingo(list.subList(i, i+5)));
		}
		return bingoList;
	}


	public static void main(String[] args) throws IOException {
		Day4 day = new Day4();
		day.run();
	}
	
}
