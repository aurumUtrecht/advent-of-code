package days2021;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import utils.BinaryHelper;
import utils.FileReaderHelper;

/**
 * Day 3 of Advent of Code 2021
 *
 */
public final class Day3 {

	private void run() throws IOException {
		var list = FileReaderHelper.readFile("input2021/day3");
		var charMatrix = getBitStrings(list);
		var anwser = method1(charMatrix);
		System.out.println(anwser);
		var anwser2 = method2(list);
		System.out.println(anwser2);
	}

	private int method1(char[][] charMatrix) {
		var cal = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		for (var string : charMatrix) {
			for (int i = 0; i < string.length; i++) {
				if (string[i] == '1') {
					cal[i] = cal[i] + 1;
				}
			}
		}

		var middle = charMatrix.length / 2;

		var gamma = new StringBuilder();
		var epsilon = new StringBuilder();

		for (var i : cal) {
			if (i > middle) {
				gamma.append("1");
				epsilon.append("0");
			} else {
				gamma.append("0");
				epsilon.append("1");
			}
		}

		var gammaNr = BinaryHelper.binaryToNumber(gamma.toString());
		var epsilonNr = BinaryHelper.binaryToNumber(epsilon.toString());

		return gammaNr * epsilonNr;
	}

	private int method2(List<String> list) {
		var oxygen = BinaryHelper.binaryToNumber(getFinalString(list, true));
		var co2 = BinaryHelper.binaryToNumber(getFinalString(list, false));
		return oxygen * co2;

	}
	
	private String getFinalString(List<String> list, boolean mostCommon) {
		var currentList = list;
		var columnindex = 0;
		while (currentList.size() > 1) {
			var currentColumnIndex = columnindex++;
			var useChar = getCharColumn(getBitStrings(currentList), currentColumnIndex, mostCommon);
			currentList = currentList.stream()
					.filter(l -> l.charAt(currentColumnIndex) == useChar)
					.collect(Collectors.toList());
		}

		return currentList.get(0);
	}

	private char[][] getBitStrings(List<String> list) {
		return list.stream().map(s -> s.toCharArray()).toArray(char[][]::new);
	}

	private char getCharColumn(char[][] charMatrix, int column, boolean mostCommon) {
		var count0 = 0;
		var count1 = 0;

		for (int i = 0; i < charMatrix.length; i++) {
			if (charMatrix[i][column] == '0') {
				count0++;
			} else {
				count1++;
			}
		}

		if (count0 == count1) {
			return mostCommon ? '1' : '0';
		}
		
		if (mostCommon) {
			return count0 > count1 ? '0' : '1';
		} else {
			return count0 < count1 ? '0' : '1';
		}
	}

	public static void main(String[] args) throws IOException {
		Day3 day = new Day3();
		day.run();
	}
}
