package days2021;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day X of Advent of Code 2021
 *
 */
public final class DefaultDay {

	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2021/day1");
		int anwser = method1(list);
		System.out.println(anwser);
		int anwser2 = method2(list);
		System.out.println(anwser2);
	}

	private int method1(List<Integer> list) {
		// TODO Auto-generated method stub
		return -1;
	}
	
	private int method2(List<Integer> list) {
		// TODO Auto-generated method stub
		return -1;
	}



	public static void main(String[] args) throws IOException {
		DefaultDay day = new DefaultDay();
		day.run();
	}
}
