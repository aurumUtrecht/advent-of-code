package days2021;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import days2021.helpclasses.SegmentReader;
import utils.FileReaderHelper;

/**
 * Day X of Advent of Code 2021
 *
 */
public final class Day8 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2021/day8");
		long anwser = method1(list);
		System.out.println(anwser);
		long anwser2 = method2(list);
		System.out.println(anwser2);
	}

	private long method1(List<String> list) {
		return list.stream().map(o -> o.split("\\|")[1])
				.flatMap(l -> Arrays.stream(l.split(" ")))
				.filter(f -> !f.isEmpty())
				.filter(n -> n.length() == 2 || n.length() == 3 || n.length() == 4 || n.length() == 7)
				.count();
	}

	private long method2(List<String> list) {
		SegmentReader reader = new SegmentReader(list);
		return reader.getSegments().stream().mapToLong(s -> s.getOutputValue()).sum();
	}

	public static void main(String[] args) throws IOException {
		Day8 day = new Day8();
		day.run();
	}
}
