package days2021;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 9 of Advent of Code 2021
 *
 */
public final class Day9 {

	private void run() throws IOException {
		int[][] matrix = FileReaderHelper.readFilesToIntMatrix("input2021/day9");
		int anwser = method1(matrix);
		System.out.println(anwser);
//		int anwser2 = method2(list);
//		System.out.println(anwser2);
	}

	private int method1(int[][] matrix) {
		int amount = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				int nr = matrix[i][j];
				int nr1 = i > 0 ?  matrix[i -1][j] : Integer.MAX_VALUE;
				int nr2 = i < matrix.length - 1 ? matrix[i + 1][j] : Integer.MAX_VALUE;
				int nr3 = j > 0 ?  matrix[i][j -1] : Integer.MAX_VALUE;
				int nr4 = j < matrix[i].length - 1 ? matrix[i][j+1] : Integer.MAX_VALUE;
				if( nr < nr1 && nr < nr2 && nr < nr3 && nr < nr4) {
					amount += nr + 1;
					System.out.println(String.format("%s: %s-%s-%s-%s", nr,nr1,nr2,nr3,nr4));
					System.out.println(amount);
				}
			}
			
		}
		return amount;
	}
	
	private int method2(List<Integer> list) {
		// TODO Auto-generated method stub 225 to low
		return -1;
	}



	public static void main(String[] args) throws IOException {
		Day9 day = new Day9();
		day.run();
	}
}
