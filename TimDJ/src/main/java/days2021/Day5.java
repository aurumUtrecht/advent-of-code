package days2021;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.FileReaderHelper;
import utils.Line;
import utils.Point;

/**
 * Day 5 of Advent of Code 2021
 *
 */
public final class Day5 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2021/day5");
		List<Line> lines = createLines(list);
		int anwser = method1(lines);
		System.out.println(anwser);
		int anwser2 = method2(lines);
		System.out.println(anwser2);
	}

	private int method1(List<Line> list) {
		int[][] matrix = new int[1000][1000];

		for (Line line : list) {
			if (line.getStartPoint().getX() == line.getEndPoint().getX()
					|| line.getStartPoint().getY() == line.getEndPoint().getY()) {
				addLine(matrix, line);
			}

		}

		int amountOverlap = 0;

		for (int[] is : matrix) {
			for (int is2 : is) {
				if (is2 > 1) {
					amountOverlap++;
				}
			}
		}

		return amountOverlap;
	}

	private int method2(List<Line> list) {
		int[][] matrix = new int[1000][1000];

		for (Line line : list) {
				addLine(matrix, line);
		}

		int amountOverlap = 0;

		for (int[] is : matrix) {
			for (int is2 : is) {
				if (is2 > 1) {
					amountOverlap++;
				}
			}
		}

		return amountOverlap;
	}

	private void addLine(int[][] matrix, Line line) {
		int currentIndexX = line.getStartPoint().getX();
		int currentIndexY = line.getStartPoint().getY();
		int endIndexX = line.getEndPoint().getX();
		int endIndexY = line.getEndPoint().getY();

		boolean endIndexNotReached = true;

		while (endIndexNotReached) {
			matrix[currentIndexX][currentIndexY]++;
			if (currentIndexX == endIndexX && currentIndexY == endIndexY) {
				endIndexNotReached = false;
			} else {
				currentIndexX = moveIndex(currentIndexX, endIndexX);
				currentIndexY = moveIndex(currentIndexY, endIndexY);
			}
		}
	}

	private int moveIndex(int currentIndex, int endIndex) {
		if (currentIndex == endIndex) {
			return currentIndex;
		}

		return currentIndex < endIndex ? currentIndex + 1 : currentIndex - 1;

	}

	private List<Line> createLines(List<String> list) {
		List<Line> lines = new ArrayList<>();
		for (String string : list) {
			String points[] = string.split(" -> ");
			String[] startPoint = points[0].split(",");
			String[] endPoint = points[1].split(",");
			lines.add(new Line(new Point(startPoint[0], startPoint[1]), new Point(endPoint[0], endPoint[1])));
		}
		return lines;
	}

	public static void main(String[] args) throws IOException {
		Day5 day = new Day5();
		day.run();
	}
}
