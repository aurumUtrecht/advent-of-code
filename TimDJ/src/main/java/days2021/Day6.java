package days2021;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.FileReaderHelper;

/**
 * Day 6 of Advent of Code 2021
 *
 */
public final class Day6 {
	private static List<Fish> pool = new ArrayList<>();
	private static List<Fish> newwFish = new ArrayList<>();

	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2021/day6");
		int anwser = method1(list);
		System.out.println(anwser);
		long anwser2 = method2(list);
		System.out.println(anwser2);
	}

	private int method1(List<Integer> list) {
		list.stream().forEach(i -> pool.add(new Fish(i)));

		for (int j = 0; j < 80; j++) {
			pool.addAll(newwFish);
			newwFish.clear();
			pool.forEach(f -> f.nextDay());
		}
		return pool.size();
	}

	private long method2(List<Integer> list) {
		Map<Integer, Long> fishMapping = new HashMap<>();

		for (int i = 0; i <= 8; i++) {
			int nr = i;
			fishMapping.put(i, list.stream().filter(j -> j.equals(nr)).count());
		}

		for (int i = 0; i < 256; i++) {
			long age0 = fishMapping.get(0);
			fishMapping.put(0, fishMapping.get(1));
			fishMapping.put(1, fishMapping.get(2));
			fishMapping.put(2, fishMapping.get(3));
			fishMapping.put(3, fishMapping.get(4));
			fishMapping.put(4, fishMapping.get(5));
			fishMapping.put(5, fishMapping.get(6));
			fishMapping.put(6, (fishMapping.get(7) + age0));
			fishMapping.put(7, fishMapping.get(8));
			fishMapping.put(8, age0);
		}

		return fishMapping.values().stream().mapToLong(l -> l).sum();
	}

	public static void main(String[] args) throws IOException {
		Day6 day = new Day6();
		day.run();
	}

	private final class Fish {

		private int days;

		public Fish() {
			this(9);
		}

		public Fish(int days) {
			this.days = days;
		}

		public void nextDay() {
			days--;
			if (days == 0) {
				days = 7;
				newwFish.add(new Fish());
			}
		}

	}
}
