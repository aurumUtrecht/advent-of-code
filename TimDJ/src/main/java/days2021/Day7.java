package days2021;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import utils.CalculationHelper;
import utils.FileReaderHelper;

/**
 * Day X of Advent of Code 2021
 *
 */
public final class Day7 {

	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2021/day7");
		int anwser = method(list, (int a, int b) -> calculateFuel(a, b));
		System.out.println(anwser);
		int anwser2 = method(list, (int a, int b) -> calculateFuel2(a, b));
		System.out.println(anwser2);
	}

	private int method(List<Integer> list, CalculateFuel fuel) {
		int lowestFuelCost = Integer.MAX_VALUE;
		for (int i = 0; i < 1500; i++) {
			int position = i;
			int fuelCost = list.stream().map(c -> fuel.calculate(c, position)).mapToInt(j -> j).sum();
			if (fuelCost < lowestFuelCost) {
				lowestFuelCost = fuelCost;
			}
		}
		return lowestFuelCost;
	}

	private int calculateFuel(int position1, int postition2) {
		return CalculationHelper.calculateDifference(position1, postition2);
	}

	private int calculateFuel2(int position1, int postition2) {
		int distance = CalculationHelper.calculateDifference(position1, postition2);
		return IntStream.range(1, distance + 1).sum();
	}

	public static void main(String[] args) throws IOException {
		Day7 day = new Day7();
		day.run();
	}

	@FunctionalInterface
	public interface CalculateFuel {
		int calculate(int a, int b);
	}
}
