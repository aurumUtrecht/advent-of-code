package days2021.helpclasses;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Preconditions;

public final class Bingo {

	private int[] numbers;
	private boolean[] marked;

	public Bingo(List<String> numbersString) {
		Preconditions.checkArgument(numbersString.size() == 5);

		this.numbers = numbersString.stream()
				.flatMap(s ->  Arrays.stream(s.split(" ")))
				.filter(l -> !l.isEmpty())
				.mapToInt(i -> Integer.parseInt(i))
				.toArray();

		marked = new boolean[25];
		for (int i = 0; i < 25; i++) {
			marked[i] = false;
		}

	}
	
	public void mark(int number) {
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == number) {
				marked[i] = true; 
			}
		}
	}
	
	public boolean bingo() {
		return (marked[0] && marked[1] && marked[2] && marked[3] && marked[4])
				|| (marked[5] && marked[6] && marked[7] && marked[8] && marked[9])
				|| (marked[10] && marked[11] && marked[12] && marked[13] && marked[14])
				|| (marked[15] && marked[16] && marked[17] && marked[18] && marked[19])
				|| (marked[20] && marked[21] && marked[22] && marked[23] && marked[24])
				|| (marked[0] && marked[5] && marked[10] && marked[15] && marked[20])
				|| (marked[1] && marked[6] && marked[11] && marked[16] && marked[21])
				|| (marked[2] && marked[7] && marked[12] && marked[17] && marked[22])
				|| (marked[3] && marked[8] && marked[13] && marked[18] && marked[23])
				|| (marked[4] && marked[9] && marked[14] && marked[19] && marked[24]);
	}

	public int countScore() {
		int sum = 0;

		for (int i = 0; i < marked.length; i++) {
			if (!marked[i]) {
				sum += numbers[i];
			}
		}
		return sum;
	}
}
