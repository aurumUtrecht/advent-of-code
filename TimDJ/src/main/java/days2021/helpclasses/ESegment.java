package days2021.helpclasses;

public enum ESegment {
	TOP,
	TOP_LEFT,
	TOP_RIGHT,
	MIDDLE,
	BOTTOM_LEFT,
	BOTTOM_RIGHT,
	BOTTOM;
}