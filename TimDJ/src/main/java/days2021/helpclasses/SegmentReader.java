package days2021.helpclasses;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class SegmentReader {

	List<Segment> segments;

	public SegmentReader(List<String> input) {
		readSegments(input);
	}

	private void readSegments(List<String> input) {
		this.segments = input.stream().map(l -> createSegment(l)).collect(Collectors.toList());
	}

	private Segment createSegment(String line) {
		String[] values = line.split("\\|");
		String[] signalPattern = values[0].split(" ");
		String[] outputValue = values[1].split(" ");
		
		Map<ESegment, Character> map = getMapping(signalPattern);
		long signalPatternNumber = getNumber(map, signalPattern); 
		long outputValueNumber = getNumber(map, outputValue);
		
		return new Segment(line, map, signalPatternNumber, outputValueNumber);
	}

	private Map<ESegment, Character> getMapping(String[] numbers) {
		Map<ESegment, Character> map = new HashMap<>();

		String one = Arrays.stream(numbers).filter(s -> s.length() == 2).findFirst().get();
		String four = Arrays.stream(numbers).filter(s -> s.length() == 4).findFirst().get();
		String seven = Arrays.stream(numbers).filter(s -> s.length() == 3).findFirst().get();
		String eight = Arrays.stream(numbers).filter(s -> s.length() == 7).findFirst().get();
		List<String> sixNineZero = Arrays.stream(numbers).filter(s -> s.length() == 6).collect(Collectors.toList());	

		// Step 1: compare 7 & 1 --> Get Top
		char top = seven.chars().mapToObj(c -> (char)c).filter(c -> !one.contains("" + c)).findFirst().get();
		map.put(ESegment.TOP, top);

		// Step 2: 6,9,0 with 1 --> Get Top Right & Bottom Right
		
		List<Character> openSegements = sixNineZero.stream()
				.map(s -> eight.chars()
						.mapToObj(c -> (char)c)
						.filter(c -> !s.contains("" + c))
						.findFirst().get())
				.collect(Collectors.toList());
		
		if (one.charAt(0) == openSegements.get(0) || one.charAt(0) == openSegements.get(1)
				|| one.charAt(0) == openSegements.get(2)) {
			map.put(ESegment.TOP_RIGHT, one.charAt(0));
			map.put(ESegment.BOTTOM_RIGHT, one.charAt(1));
		} else {
			map.put(ESegment.TOP_RIGHT, one.charAt(1));
			map.put(ESegment.BOTTOM_RIGHT, one.charAt(0));
		}

		// Step 3: Compare 4 & 8 minus TOP --> Get possible Bottom left and Bottom
		List<Character> bottomLeftChars = eight.chars()
				.mapToObj(c -> (char)c)
				.filter(c -> !four.contains("" + c))
				.filter(c -> c != top).collect(Collectors.toList());
		
		// Step 4: compare 6 & 9 & minus known --> Get Possible Middle and BotTom Left
		List<Character> middleLeft = openSegements.stream()
				.filter(c -> c != map.get(ESegment.TOP_RIGHT))
				.collect(Collectors.toList());
				
		// Step 5: Compare Step 3 & 4 --> Get Middle, Bottom Left and Bottom

		if (bottomLeftChars.contains(middleLeft.get(0))) {
			map.put(ESegment.BOTTOM_LEFT, middleLeft.get(0));
			map.put(ESegment.MIDDLE, middleLeft.get(1));
		} else {
			map.put(ESegment.BOTTOM_LEFT, middleLeft.get(1));
			map.put(ESegment.MIDDLE, middleLeft.get(0));
		}

		bottomLeftChars.remove(map.get(ESegment.BOTTOM_LEFT));
		map.put(ESegment.BOTTOM, bottomLeftChars.get(0));

		// Step 6: 8 minus known --> Get Top Left

		map.put(ESegment.TOP_LEFT, eight.chars()
				.mapToObj(c -> (char)c)
				.filter(c -> !map.containsValue(c))
				.findFirst()
				.get());
		return map;

	}
	
	private long getNumber(Map<ESegment, Character> map, String[] signalPattern) {
		StringBuilder sb = new StringBuilder();
		for (String string : signalPattern) {
			int length = string.length();
			switch (length) {
			case 2:
				sb.append("1");
				break;
			case 3:
				sb.append("7");
				break;
			case 4:
				sb.append("4");
				break;
			case 5:
				if (string.contains("" + map.get(ESegment.BOTTOM_LEFT))) {
					sb.append("2");
				} else if (string.contains("" + map.get(ESegment.TOP_LEFT))) {
					sb.append("5");
				} else {
					sb.append("3");
				}
				break;
			case 6:
				if (!string.contains("" + map.get(ESegment.MIDDLE))) {
					sb.append("0");
				} else if (!string.contains("" + map.get(ESegment.BOTTOM_LEFT))) {
					sb.append("9");
				} else {
					sb.append("6");
				}
				break;
			case 7:
				sb.append("8");
				break;

			}
		}
		return Long.parseLong(sb.toString());
	}

	public List<Segment> getSegments() {
		return segments;
	}

}
