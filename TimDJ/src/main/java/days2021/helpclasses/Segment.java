package days2021.helpclasses;

import java.util.Map;

public final class Segment {

	private final String input;
	private final Map<ESegment, Character> mapping;
	private final long signalPattern;
	private final long outputValue;

	public Segment(String input, Map<ESegment, Character> mapping, long signalPattern, long outputValue) {
		this.input = input;
		this.mapping = mapping;
		
		this.signalPattern = signalPattern;
		this.outputValue = outputValue;;
	}

	public String getInput() {
		return input;
	}
	
	public Map<ESegment, Character> getMapping() {
		return mapping;
	}

	public long getSignalPattern() {
		return signalPattern;
	}

	public long getOutputValue() {
		return outputValue;
	}
}