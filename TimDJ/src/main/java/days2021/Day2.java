package days2021;

import java.io.IOException;
import java.util.List;

import utils.EDirection;
import utils.FileReaderHelper;

/**
 * Day 2 of Advent of Code 2021
 *
 */
public final class Day2 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2021/day2");
		int anwser = method1(list);
		System.out.println(anwser);
		int anwser2 = method2(list);
		System.out.println(anwser2);
	}

	private int method1(List<String> list) {
		int depth = 0;
		int horizontal = 0;

		for (String string : list) {
			EDirection direction = getDirection(string);
			int value = getValue(string);

			switch (direction) {
			case FORWARD:
				horizontal += value;
				break;
			case DOWN:
				depth += value;
				break;
			case UP:
				depth -= value;
				break;
			}

		}
		return depth * horizontal;
	}

	private int method2(List<String> list) {
		int depth = 0;
		int horizontal = 0;
		int aim = 0;

		for (String string : list) {
			EDirection direction = getDirection(string);
			int value = getValue(string);

			switch (direction) {
			case FORWARD:
				horizontal += value;
				depth += (aim * value);
				break;
			case DOWN:
				aim += value;
				break;
			case UP:
				aim -= value;
				break;
			}

		}
		return depth * horizontal;
	}

	private EDirection getDirection(String string) {
		return EDirection.getDirection(string.split(" ")[0]);
	}

	private int getValue(String string) {
		return Integer.parseInt(string.split(" ")[1]);
	}

	public static void main(String[] args) throws IOException {
		Day2 day = new Day2();
		day.run();
	}
}
