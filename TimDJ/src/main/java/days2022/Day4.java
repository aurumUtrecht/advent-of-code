package days2022;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 4 of Advent of Code 2022
 */
public final class Day4 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2022/day4");
		long anwser = part1(list);
		System.out.println(anwser);
		long anwser2 = part2(list);
		System.out.println(anwser2);
	}

	private long part1(List<String> list) {
		return list.stream().filter(a -> fullyOverlaps(splitAssigments(a))).count();
	}
	
	private long part2(List<String> list) {
		return list.stream().filter(a -> partlyOverlaps(splitAssigments(a))).count();
	}
	
	private int[] splitAssigments(String assigments) {
		String[] assigment = assigments.split(",");
		String[] elf1Assigments = assigment[0].split("-");
		String[] elf2Assigments = assigment[1].split("-");
		int[] splitAssigments = new int[] { Integer.parseInt(elf1Assigments[0]), Integer.parseInt(elf1Assigments[1]),
				Integer.parseInt(elf2Assigments[0]), Integer.parseInt(elf2Assigments[1]) };
		return splitAssigments;
	}
	
	private boolean fullyOverlaps(int[] assigments) {
	 int elf1AssigmentLow = assigments[0];
	 int elf1AssigmentHigh = assigments[1];
	 int elf2AssigmentLow = assigments[2];
	 int elf2AssigmentHigh = assigments[3];
	 
	 if (elf1AssigmentLow == elf2AssigmentLow || elf1AssigmentHigh == elf2AssigmentHigh) {
		 return true;
	 }
	 
	 if (elf1AssigmentLow <= elf2AssigmentLow && elf1AssigmentHigh >= elf2AssigmentHigh) {
		 return true;
	 }
	 
	 if (elf2AssigmentLow <= elf1AssigmentLow && elf2AssigmentHigh >= elf1AssigmentHigh) {
		 return true;
	 }
	 
	 return false;
	}

	private boolean partlyOverlaps(int[] assigments) {
		 int elf1AssigmentLow = assigments[0];
		 int elf1AssigmentHigh = assigments[1];
		 int elf2AssigmentLow = assigments[2];
		 int elf2AssigmentHigh = assigments[3];
		 
		 if (elf1AssigmentLow == elf2AssigmentLow || elf1AssigmentHigh == elf2AssigmentHigh) {
			 return true;
		 }
		 
		 if (elf1AssigmentLow <= elf2AssigmentLow && elf1AssigmentHigh >= elf2AssigmentLow) {
			 return true;
		 }
		 
		 if (elf2AssigmentLow <= elf1AssigmentLow && elf2AssigmentHigh >= elf1AssigmentLow) {
			 return true;
		 }
		 
		 return false;
		}	
	
	public static void main(String[] args) throws IOException {
		Day4 day = new Day4();
		day.run();
	}
}