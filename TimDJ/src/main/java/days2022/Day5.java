package days2022;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import utils.FileReaderHelper;

/**
 * Day 5 of Advent of Code 2022
 */
public final class Day5 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2022/day5");
		int index = list.indexOf("");
		List<String> setup = list.subList(0, index);
		List<String> orders = list.subList(index + 1, list.size());

		Map<Integer, Stack<Crate>> stacks = createStacks(setup);
		part1(stacks, orders);

		String answer = getTopStacks(stacks);
		System.out.println(answer);

		// Refresh startOrder
		Map<Integer, Stack<Crate>> stacks2 = createStacks(setup);
		part2(stacks2, orders);

		String answer2 = getTopStacks(stacks2);
		System.out.println(answer2);
	}

	private String getTopStacks(Map<Integer, Stack<Crate>> stacks) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Integer, Stack<Crate>> entry : stacks.entrySet()) {
			sb.append(entry.getValue().peek().value);
		}
		return sb.toString();
	}

	private Map<Integer, Stack<Crate>> createStacks(List<String> setup) {
		int numberOfStacks = (setup.get(setup.size() - 1).length() + 1) / 4;

		Map<Integer, Stack<Crate>> stacks = new HashMap<>();

		for (int i = 1; i <= numberOfStacks; i++) {
			Stack<Crate> stack = new Stack<>();
			int indexStack = ((i - 1) * 4) + 1;

			for (int j = setup.size() - 2; j >= 0; j--) {
				if (setup.get(j).length() >= indexStack) {
					char crateChar = setup.get(j).charAt(indexStack);
					if (crateChar != ' ') {
						stack.push(new Crate(crateChar));
					}
				}
			}
			stacks.put(i, stack);
		}
		return stacks;
	}

	private void part1(Map<Integer, Stack<Crate>> stacks, List<String> list) {
		for (String order : list) {
			String[] splitorder = order.split(" ");
			int amount = Integer.parseInt(splitorder[1]);
			int from = Integer.parseInt(splitorder[3]);
			int to = Integer.parseInt(splitorder[5]);

			for (int i = 0; i < amount; i++) {
				stacks.get(to).push(stacks.get(from).pop());
			}
		}
	}

	private void part2(Map<Integer, Stack<Crate>> stacks, List<String> list) {
		for (String order : list) {
			String[] splitorder = order.split(" ");
			int amount = Integer.parseInt(splitorder[1]);
			int from = Integer.parseInt(splitorder[3]);
			int to = Integer.parseInt(splitorder[5]);

			List<Crate> subset = stacks.get(from).subList(stacks.get(from).size() - amount, stacks.get(from).size());
			stacks.get(to).addAll(subset);
			subset.clear();
		}
	}

	public record Crate(char value) {
	}

	public static void main(String[] args) throws IOException {
		Day5 day = new Day5();
		day.run();
	}
}