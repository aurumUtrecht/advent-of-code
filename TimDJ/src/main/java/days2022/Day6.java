package days2022;

import java.io.IOException;

import utils.FileReaderHelper;

/**
 * Day 6 of Advent of Code 2022
 */
public final class Day6 {

	private void run() throws IOException {
		String input = FileReaderHelper.readFileSingelString("input2022/day6");
		int answer = findUniqueIndex(4, input, 4);
		System.out.println(answer);
		int answer2 = findUniqueIndex(14, input, 14);
		System.out.println(answer2);
	}

	private int findUniqueIndex(int index, String input, int unique) {
		if (input.substring(index - unique, index).chars().distinct().count() == unique) {
			return index;
		}
		return findUniqueIndex(index + 1, input, unique);
	}

	public static void main(String[] args) throws IOException {
		Day6 day = new Day6();
		day.run();
	}
}