package days2022;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import utils.CalculationHelper;
import utils.EDirection;
import utils.FileReaderHelper;
import utils.Point;

/**
 * Day 6 of Advent of Code 2022
 */
public final class Day9 {

	private void run() throws IOException {
		List<Motion> motions = getMotions("input2022/day9");
		int answer = part1(motions);
		System.out.println(answer);
		int answer2 = part2(motions);
		System.out.println(answer2);
	}
	
	private int part1(List<Motion> motions) {
		Set<Point> visitedPoints = new HashSet<>();
		// Add starting point
		visitedPoints.add(new Point(0, 0));

		int headX = 0;
		int headY = 0;
		int tailX = 0;
		int tailY = 0;
		
		for (Motion motion : motions) {
			for (int i = 0; i < motion.steps; i++) {
				switch (motion.direction) {
				case UP -> headY++;
				case DOWN -> headY--;
				case RIGHT -> headX++;
				case LEFT -> headX--;
				default -> throw new IllegalArgumentException("invalid direction " + motion.direction);
				}

				// Check if tail should move.
				if (CalculationHelper.calculateDifference(headX, tailX) > 1 || CalculationHelper.calculateDifference(headY, tailY) > 1) {
					// Check we should move diagonally
					if (CalculationHelper.calculateDifference(headX, tailX) + CalculationHelper.calculateDifference(headY, tailY) > 2) {
						// find digonal direction
						if (headX > tailX && headY > tailY) {
							tailX++;
							tailY++;
						} else if (headX > tailX && headY < tailY) {
							tailX++;
							tailY--;
						} else if (headX < tailX && headY > tailY) {
							tailX--;
							tailY++;
						} else if (headX < tailX && headY < tailY) {
							tailX--;
							tailY--;
						} else {
							throw new IllegalArgumentException(
									String.format("can't find direction to move tail head x:%d y:%d tail  x:%d y:%d",
											headX, headY, tailX, tailY));
						}
					} else {
						// move same direction as head
						switch (motion.direction) {
						case UP -> tailY++;
						case DOWN -> tailY--;
						case RIGHT -> tailX++;
						case LEFT -> tailX--;
						default -> throw new IllegalArgumentException("invalid direction " + motion.direction);
						}
					}
					visitedPoints.add(new Point(tailX, tailY));
				}
			}
		}
		return visitedPoints.size();
	}
	
	private int part2(List<Motion> motions) {
		Map<Integer, Knot> knots = new HashMap<>();
		knots.put(1, new Knot());
		knots.put(2, new Knot());
		knots.put(3, new Knot());
		knots.put(4, new Knot());
		knots.put(5, new Knot());
		knots.put(6, new Knot());
		knots.put(7, new Knot());
		knots.put(8, new Knot());
		knots.put(9, new Knot());

		int headX = 0;
		int headY = 0;
		
		for (Motion motion : motions) {
			// Move head
			for (int i = 0; i < motion.steps; i++) {
				switch (motion.direction) {
				case UP -> headY++;
				case DOWN -> headY--;
				case RIGHT -> headX++;
				case LEFT -> headX--;
				default -> throw new IllegalArgumentException("invalid direction " + motion.direction);
				}

				// Move knots
				Point predecessor = new Point(headX, headY);
				for (int j = 1; j <= 9; j++) {
					predecessor = knots.get(j).newPointPredecessor(predecessor);
				}
			}
		}
		return knots.get(9).visitedPoints.size();
	}

	private List<Motion> getMotions(String fileName) throws IOException {
		try (InputStream inputStream = FileReaderHelper.class.getClassLoader().getResourceAsStream((fileName))) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			return reader.lines()
					.map(l -> new Motion(EDirection.getDirection(l.charAt(0)), Integer.parseInt(l.split(" ")[1])))
					.collect(Collectors.toList());
		}
	}
	
	public record Motion(EDirection direction, int steps) {
	}
	
	private class Knot {
		private int locationX = 0;
		private int locationY = 0;
		private Set<Point> visitedPoints = new HashSet<>();

		Knot() {
			visitedPoints.add(new Point(0, 0));

		}

		Point newPointPredecessor(Point point) {
			System.out.println(String.format("Check Moving Tail head x:%d y:%d tail  x:%d y:%d", point.getX(),
					point.getY(), locationX, locationY));
			// Check if tail should move.
			if (CalculationHelper.calculateDifference(point.getX(), locationX) > 1
					|| CalculationHelper.calculateDifference(point.getY(), locationY) > 1) {
				// Check we should move diagonally
				if (CalculationHelper.calculateDifference(point.getX(), locationX)
						+ CalculationHelper.calculateDifference(point.getY(), locationY) > 2) {
					// find digonal direction
					if (point.getX() > locationX && point.getY() > locationY) {
						locationX++;
						locationY++;
					} else if (point.getX() > locationX && point.getY() < locationY) {
						locationX++;
						locationY--;
					} else if (point.getX() < locationX && point.getY() > locationY) {
						locationX--;
						locationY++;
					} else if (point.getX() < locationX && point.getY() < locationY) {
						locationX--;
						locationY--;
					} else {
						throw new IllegalArgumentException(
								String.format("can't find direction to move tail head x:%d y:%d tail  x:%d y:%d",
										point.getX(), point.getY(), locationX, locationY));
					}
				} else {
					
					// find direction to move
					if (CalculationHelper.calculateDifference(point.getX(), locationX) > 1) {
						if (point.getX() > locationX) {
							locationX++;
						} else {
							locationX--;
						}
					} else {
						if (point.getY() > locationY) {
							locationY++;
						} else {
							locationY--;
						}
					}
					System.out.println(String.format("moved tail head x:%d y:%d tail  x:%d y:%d", point.getX(),
							point.getY(), locationX, locationY));
				}
				visitedPoints.add(new Point(locationX, locationY));
			}
			return new Point(locationX, locationY);
		}
	}
	public static void main(String[] args) throws IOException {
		Day9 day = new Day9();
		day.run();
	}
}