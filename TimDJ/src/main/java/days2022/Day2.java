package days2022;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.FileReaderHelper;

/**
 * Day 2 of Advent of Code 2022
 */
public final class Day2 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2022/day2");
		int anwser = getResult(list, method1());
		System.out.println(anwser);
		int anwser2 = getResult(list, method2());
		System.out.println(anwser2);
	}

	private int getResult(List<String> list, Map<String, Integer> resultMap) {
		int totalPoints = 0;
		for (String string : list) {
			totalPoints += resultMap.get(string.replace(" ", ""));
		}
		return totalPoints;
	}
	private Map<String, Integer> method1() {
		Map<String, Integer> resultMap = new HashMap<>();
		resultMap.put("AX", 4);
		resultMap.put("AY", 8);
		resultMap.put("AZ", 3);
		resultMap.put("BX", 1);
		resultMap.put("BY", 5);
		resultMap.put("BZ", 9);
		resultMap.put("CX", 7);
		resultMap.put("CY", 2);
		resultMap.put("CZ", 6);
		return resultMap;
	}

	private Map<String, Integer> method2() {
		Map<String, Integer> resultMap = new HashMap<>();
		resultMap.put("AX", 3);
		resultMap.put("AY", 4);
		resultMap.put("AZ", 8);
		resultMap.put("BX", 1);
		resultMap.put("BY", 5);
		resultMap.put("BZ", 9);
		resultMap.put("CX", 2);
		resultMap.put("CY", 6);
		resultMap.put("CZ", 7);
		return resultMap;
	}

	public static void main(String[] args) throws IOException {
		Day2 day = new Day2();
		day.run();
	}
}