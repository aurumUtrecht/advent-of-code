package days2022;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 3 of Advent of Code 2022
 */
public final class Day3 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2022/day3");
		int anwser = part1(list);
		System.out.println(anwser);
		int anwser2 = part2(list);
		System.out.println(anwser2);
	}

	private int part1(List<String> list) {
		return list.stream().map(r -> findItem(r)).mapToInt(c -> getValue(c)).sum();
	}
	
	private int part2(List<String> list) {
		int sum = 0;
		for (int i = 0; i < list.size(); i = i + 3) {
			char common = findCommon(list.get(i), list.get(i + 1), list.get(i + 2));
			sum += getValue(common);
		}
		return sum;
	}

	private char findItem(String ruckstack) {
		String compartment1 = ruckstack.substring(0, ruckstack.length() / 2);
		String compartment2 = ruckstack.substring(ruckstack.length() / 2);
		
		return (char)compartment1.chars()
				.filter(x -> compartment2.chars().anyMatch(c -> x == c))
				.findFirst()
				.getAsInt();
	}
	
	private int getValue(Character c) {
		if (Character.isUpperCase(c)) {
			return Character.getNumericValue(c) + 17;
		}
		if (Character.isLowerCase(c)) {
			return Character.getNumericValue(c) - 9;
		}
		throw new IllegalStateException("Invalid char: " + c);
	}

	private char findCommon(String elf1, String elf2, String elf3) {
		return (char)elf1.chars()
				.filter(x -> elf2.chars().anyMatch(c -> x == c))
				.filter(x -> elf3.chars().anyMatch(c -> x == c))
				.findFirst()
				.getAsInt();
	}

	public static void main(String[] args) throws IOException {
		Day3 day = new Day3();
		day.run();
	}
}