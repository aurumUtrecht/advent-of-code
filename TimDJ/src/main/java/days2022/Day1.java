package days2022;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Strings;

import utils.FileReaderHelper;

/**
 * Day 1 of Advent of Code 2022
 */
public final class Day1 {
	
	private void run2() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2022/day1");
		List<List<Integer>> listOfLists = createListofLists(list);
		int anwser = listOfLists.stream().mapToInt(l -> l.stream().mapToInt(v -> v).sum()).max().getAsInt();;
		System.out.println(anwser);
		int anwser2 = listOfLists.stream().mapToInt(l -> l.stream().mapToInt(v -> v).sum()).boxed()
				.sorted(Collections.reverseOrder()).limit(3).mapToInt(v -> v).sum();
		System.out.println(anwser2);
	}

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2022/day1");
		List<List<Integer>> listOfLists = createListofLists(list);
		int anwser = method1(listOfLists);
		System.out.println(anwser);
		int anwser2 = method2(listOfLists);
		System.out.println(anwser2);
	}

	private List<List<Integer>> createListofLists(List<String> list) {
		List<List<Integer>> listOfLists = new ArrayList<>();
		List<Integer> newList = new ArrayList<>();
		
		for (String value : list) {
			if(Strings.isNullOrEmpty(value)) {
				listOfLists.add(newList);
				newList = new ArrayList<>();
				continue;
			}
			newList.add(Integer.parseInt(value));
		}
		listOfLists.add(newList);
		return listOfLists;
	}

	private int method1(List<List<Integer>> list) {
		return list.stream()
				.mapToInt(l -> l.stream()
						.mapToInt(v -> v)
						.sum())
				.max()
				.getAsInt();
	}
	
	private int method2(List<List<Integer>> list) {
		return list.stream()
				.mapToInt(l -> l.stream()
						.mapToInt(v -> v)
						.sum())
				.boxed()
				.sorted(Collections.reverseOrder())
				.limit(3)
				.mapToInt(v -> v)
				.sum();
	}

	public static void main(String[] args) throws IOException {
		Day1 day = new Day1();
		day.run();
	}
}
