package days2022;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 7 of Advent of Code 2022
 */
public final class Day7 {

	private void run() throws IOException {
		List<String> input = FileReaderHelper.readFile("input2022/day7");
		ITraversable root = createFileSystem(input);
		long answer = part1(root);
		System.out.println(answer);
		int answer2 = part2(root);
		System.out.println(answer2);
	}

	private long part1(ITraversable root) {
		return getSize(root);
	}
	
	private int part2(ITraversable root) {
		int totalFree = 70000000 - root.getSize();
		int required = 30000000 - totalFree;
		
		List<ITraversable> folders = findPossibleFolders(root, required);
		return folders.stream().mapToInt(f -> f.getSize()).min().getAsInt();
	}

	private List<ITraversable> findPossibleFolders(ITraversable root, int required) {
		List<ITraversable> list = new ArrayList<>();
		findSize(root, list, required);
		return list;
	}
	
	private void findSize(ITraversable element, List<ITraversable> list, int required) {
		for (ITraversable child : element.getChildren()) {
			if (child instanceof File) {
				continue;
			}
			//System.out.println(child.getSize());
			if (child.getSize() >= required) {
				System.out.println("adding");
				list.add(child);
			}
			if (child.hasChildren()) {
				findSize(child, list, required);
			}
		}
	}

	private long getSize(ITraversable element) {
		long childrenSize = 0;
		for (ITraversable child : element.getChildren()) {
			if (child instanceof File) {
				continue;
			}
			if (child.getSize() <= 100000) {
				childrenSize += child.getSize();
			}
			if (child.hasChildren()) {
				childrenSize += getSize(child);
			}
		}
		return childrenSize;
	}

	private ITraversable createFileSystem(List<String> input) {
		ITraversable root =  new Folder(null, "/");
		ITraversable currentLocation = root;

		for (String line : input) {
			if (line.charAt(0) == '$') {
				if (line.charAt(2) == 'l') {
					// Skip LS command
					continue;
				}
				String goToLocation = line.split(" ")[2];
				switch (goToLocation) {
				case ".." -> currentLocation = currentLocation.getParent();
				case "/" -> currentLocation = root;
				default -> currentLocation = currentLocation.getChildren().stream()
						.filter(t -> t.getName().equals(goToLocation)).findFirst().get();
				}
			} else {
				String[] newTraversable = line.split(" ");
				if (newTraversable[0].equals("dir")) {
					currentLocation.addChild(new Folder(currentLocation, newTraversable[1]));
				} else {
					currentLocation.addChild(new File(currentLocation, newTraversable[1], Integer.parseInt(newTraversable[0])));
				}
			}
		}
		return root;
	}

	

	public static void main(String[] args) throws IOException {
		Day7 day = new Day7();
		day.run();
	}
	
	class Folder implements ITraversable {
		final ITraversable parent;
		final String name;
		final List<ITraversable> children = new ArrayList<>();
		
		Folder(ITraversable parent, String name) {
			this.parent = parent;
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public int getSize() {
			int size = 0;
			for (ITraversable child : children) {
				size += child.getSize();
			}
			return size;
		}
		
		@Override
		public boolean hasChildren() {
			return children.size() > 0;
		}
		
		@Override
		public void addChild(ITraversable child) {
			children.add(child);
		}

		@Override
		public List<ITraversable> getChildren() {
			return children;
		}

		@Override
		public ITraversable getParent() {
			return parent;
		}
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			toIndentedString(sb, 0);
	        return sb.toString();
		}
		
		@Override
		public void toIndentedString(StringBuilder builder, int indentationLevel) {
		     for (int i = 0; i < indentationLevel; i++) {
		            builder.append(INDENT_STRING);
		        }
		        builder.append(String.format("- %s (dir)", name));
		        for (ITraversable child : children) {
		        	builder.append('\n');
		            child.toIndentedString(builder, indentationLevel + 1);
		        }
		}
	}
	
	class File implements ITraversable {
		final ITraversable parent;
		final String name;
		final int size;
		
		File(ITraversable parent, String name, int size) {
			this.parent = parent;
			this.name = name;
			this.size = size;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public int getSize() {
			return size;
		}
		
		@Override
		public boolean hasChildren() {
			return false;
		}
		
		@Override
		public void addChild(ITraversable child) {
			throw new IllegalStateException("Cannot add file to file");
		}

		@Override
		public List<ITraversable> getChildren() {
			return null;
		}

		@Override
		public ITraversable getParent() {
			return parent;
		}

		@Override
		public void toIndentedString(StringBuilder builder, int indentationLevel) {
			for (int i = 0; i < indentationLevel; i++) {
				builder.append(INDENT_STRING);
			}
			builder.append(String.format("- %s (file, size=%d)", name, size));
		}

	}
	
	interface ITraversable {
		static final String INDENT_STRING = "  ";
		
		String getName();
		
		int getSize();
		
		boolean hasChildren();
		
		void addChild(ITraversable child);
						
		List<ITraversable> getChildren();
		
		ITraversable getParent();
		
		void toIndentedString(StringBuilder builder, int indentationLevel);
	}
}