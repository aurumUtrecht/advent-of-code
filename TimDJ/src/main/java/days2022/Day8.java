package days2022;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

import utils.FileReaderHelper;

/**
 * Day 8 of Advent of Code 2022
 */
public final class Day8 {

	private void run() throws IOException {
		Stopwatch sw3 = Stopwatch.createStarted();
		int[][] input = FileReaderHelper.readFilesToIntMatrix("input2022/day8");
		System.out.println(sw3.elapsed(TimeUnit.MILLISECONDS));
		Stopwatch sw = Stopwatch.createStarted();
		int answer = part1(input);
		System.out.println(answer);
		System.out.println(sw.elapsed(TimeUnit.MILLISECONDS));
		Stopwatch sw2 = Stopwatch.createStarted();
		int answer2 = part2(input);
		System.out.println(answer2);
		System.out.println(sw2.elapsed(TimeUnit.MILLISECONDS));
	}

	private int part1(int[][] trees) {
		int amountVisible = 0;
		for (int i = 0; i < trees.length; i++) {
			for (int j = 0; j < trees.length; j++) {
				int treeSize = trees[i][j];
				if (treeVisible(treeSize, trees[i], j) || treeVisible(treeSize, getColumn(trees, j), i)) {
					amountVisible++;
				}
			}
		}
		return amountVisible;
	}
	
	private int part2(int[][] trees) {
		int highestScenicScore = 0;
		for (int i = 0; i < trees.length; i++) {
			for (int j = 0; j < trees.length; j++) {
				int treeSize = trees[i][j];
				int scenicScore = getScenicScore(treeSize, trees[i], j) * getScenicScore(treeSize, getColumn(trees, j), i);
				if (scenicScore > highestScenicScore) {
					highestScenicScore = scenicScore;
				}
			}
		}
		return highestScenicScore;
	}
	
	private int getScenicScore(int treeSize, int[] treeLine, int index) {
		int scenicScoreLeft = 0;
		int scenicScoreRight = 0;
			
		for (int i = index +1; i < treeLine.length ; i++) {
			scenicScoreRight++;
			if (treeLine[i] >= treeSize) {
				break;
			}
		}
		for (int i = index - 1; i >= 0 ; i--) {
			scenicScoreLeft++;
			if (treeLine[i] >= treeSize) {
				break;
			}
		}
		
		return scenicScoreLeft * scenicScoreRight;
	}

	private int[] getColumn(int[][] trees, int j) {
		int[] column = new int[99];
		for (int i = 0; i < trees.length; i++) {
			column[i] = trees[i][j];
		}
		return column;
	}

	private boolean treeVisible(int treeSize, int[] treeLine, int index) {
		boolean visibleLeft = true;
		boolean visibleRight = true;
		for (int i = 0; i < index; i++) {
			if (treeLine[i] >= treeSize) {
				visibleLeft = false;
			}
		}
		for (int i = treeLine.length - 1; i > index; i--) {
			if (treeLine[i] >= treeSize) {
				visibleRight = false;
			}
		}
		return visibleLeft || visibleRight;
	}

	public static void main(String[] args) throws IOException {
		Day8 day = new Day8();
		day.run();
	}
}