package default1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day1Main {

	public static void main(String[] args) {
		try {
			File input = new File("Files/InputDay1Part1.txt");
			Scanner scan = new Scanner(input);
			int count = 0;
			int previous = 0;
			int current = 0;
			while (scan.hasNextLine()){
				int data = scan.nextInt();
				System.out.println(data);
				if (current == 0) {
					current = data;
				}else {
					previous = current;
					current = data;
					if (current > previous) {
						count++;
					}
				}
			}
			System.out.println(count);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
