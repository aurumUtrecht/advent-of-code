from numpy.lib.shape_base import split
import pandas as pd
import numpy as np

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename, "r") as input:
        data = input.read().splitlines()
    return data

def bingo(mark):
    for row in mark:
        if sum(row) == 5:
            return True
    t_mark = mark.T
    for column in t_mark:
        if sum(column) == 5:
            return True
    return False

def check_bingo(marks):
    for i in range(len(marks)):
        mark = marks[i]
        if bingo(mark):
            return True, i
    return False, 0

def check_last_bingo(marks):
    all_bingo = [0 for i in range(len(marks))]
    for i in range(len(marks)):
        mark = marks[i]
        if bingo(mark):
            all_bingo[i] = 1

    if sum(all_bingo) == len(marks):
        return True, all_bingo
    return False, all_bingo

def full_column(board):
    return

input_text = load(4)
draw_numbers, boards = input_text[0], input_text[2:]

draw_numbers = draw_numbers.split(',')
draw_numbers = [int(i) for i in draw_numbers]

organized_boards = []
new_board = []
for line in boards:
    split_line = line.split()
    split_line = [int(i) for i in split_line]

    if len(split_line) == 0:
        organized_boards.append(new_board)
        new_board = []
        continue
    new_board.append(split_line)

boards = np.array(organized_boards)
marks = boards * 0
drawn_numbers = []

for no in draw_numbers:
    board_hits = np.where(boards == no)
    marks[board_hits] += 1
    check, i_mark = check_bingo(marks)
    drawn_numbers.append(no)
    if check:
        break

winning_board = boards[i_mark]
filt_winning_board = winning_board[~np.isin(winning_board, drawn_numbers)]


# print('Drawn:', drawn_numbers)
# print('Sorted drawn:', np.sort(drawn_numbers))
# print('Winning board number:', i_mark)
# print('Winning board:')
# print(winning_board)

print('Answer 1:', sum(filt_winning_board) * drawn_numbers[-1])
print('-------------------')

marks = boards * 0
drawn_numbers = []

for no in draw_numbers:
    board_hits = np.where(boards == no)
    marks[board_hits] += 1
    check, i_mark = check_last_bingo(marks)
    drawn_numbers.append(no)
    if check:
        break
    last_index = i_mark
lose_index = np.where(np.array(last_index) == 0)

losing_board = boards[lose_index]
filt_losing_board = losing_board[~np.isin(losing_board, drawn_numbers)]

print('Answer 2:', sum(filt_losing_board) * drawn_numbers[-1])

