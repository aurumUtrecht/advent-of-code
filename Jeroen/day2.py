import pandas as pd


def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename, "r") as input:
        data = input.read().splitlines()
    return data

input_text = load(2)
data = [x.split(" ") for x in input_text]


x = 0
y = 0

for row in data:
    direction = row[0]
    steps = int(row[1])

    if direction == 'forward':
        x += steps
    if direction == 'down':
        y += steps
    if direction == 'up':
        y -= steps

print('Answer 1: ', x * y)
print('---------------')

x = 0
y = 0
aim = 0

for row in data:
    direction = row[0]
    steps = int(row[1])

    if direction == 'forward':
        x += steps
        y += steps * aim
    if direction == 'down':
        aim += steps
    if direction == 'up':
        aim -= steps
print('Answer 2: ', x * y)

