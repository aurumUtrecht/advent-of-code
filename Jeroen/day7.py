import numpy as np
from scipy.optimize import minimize
import csv

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        data = list(csv_reader)
        data = [int(i) for i in data[0]]
    return data

def compute_distance(x, p):
    return np.sum(np.abs(p - x))

def compute_distance_2(x, p):
    vec = 0.5*np.abs(p - x) * (np.abs(p - x) + 1)
    tot = np.sum(vec)
    return tot

data = load(7)

# data = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]

data = np.array(data)
optimal_position = np.ones(len(data)) * int((np.max(data) - np.min(data) ) / 2)
total_distance = 99999999999999999999

for position in range(np.min(data), np.max(data)):
    p = np.ones(len(data)) * position
    new_distance = compute_distance(data, p)
    if new_distance < total_distance:
        optimal_position = position
        total_distance = new_distance

# print(optimal_position)
print('Part 1:', int(total_distance))

optimal_position = np.ones(len(data)) * int((np.max(data) - np.min(data) ) / 2)
total_distance = 99999999999999999999

for position in range(np.min(data), np.max(data)):
    p = np.ones(len(data)) * position
    new_distance = compute_distance_2(data, p)
    if new_distance < total_distance:
        optimal_position = position
        total_distance = new_distance

# print(optimal_position)
print('Part 2:', int(total_distance))