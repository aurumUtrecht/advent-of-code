import numpy as np
import csv
import re

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        data = list(csv_reader)
        data = [i for i in data]
    return data

data = load(8)
data = [x[0].split() for x in data]

#each key sorted alphabetically
signals_to_digits = {
    'abcefg': 0,
    'cf': 1,
    'acdeg': 2,
    'acdfg': 3,
    'bcdf': 4,
    'abdfg': 5,
    'abdefg': 6,
    'acf': 7,
    'abcdefg': 8,
    'abcdfg': 9
}

input_data = [x[0:10] for x in data]
output_data = [x[-4:] for x in data]
print(input_data[0])
digits = {
    0:6,
    1:2,
    2:5,
    3:5,
    4:4,
    5:5,
    6:6,
    7:3,
    8:7,
    9:6,
}

count = 0

for row in output_data:
    for signal in row:
        if len(signal) == digits[1]:
            count += 1
        if len(signal) == digits[4]:
            count += 1
        if len(signal) == digits[7]:
            count += 1
        if len(signal) == digits[8]:
            count += 1
print('Answer 1:', count)

possible_characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

solution = dict.fromkeys(possible_characters)

#  8:    
#       aaaa             
#      b    c      
#      b    c
#       dddd             
#      e    f      
#      e    f      
#       gggg

output_values = []

for row_i in range(len(input_data)):
    row_in = input_data[row_i]
    row_out = output_data[row_i]
    unique_signals = {}
    six_segment_signals = []

    # Identify digits with unique number of segments and the three digits with six segments
    for signal in row_in:
        if len(signal) == digits[1]:
            unique_signals[1] = signal
        if len(signal) == digits[4]:
            unique_signals[4] = signal
        if len(signal) == digits[7]:
            unique_signals[7] = signal
        if len(signal) == digits[8]:
            unique_signals[8] = signal
        if len(signal) == 6:
            six_segment_signals.append(signal)

    # Identify the two segments making up number 1 ('c' and 'f') and identify the top segment ('a') by checking which segment in number 7 does not occur in number 1
    cf = []
    for character in unique_signals[7] : # number 7
        if character in unique_signals[1]: # number 1
            cf.append(character)
        else:
            solution['a'] = character

    # Identify the two segments ('b' and 'd') that occur in number 4 but not in number 1
    bd = []
    for character in unique_signals[4]:
        if character not in cf:
            bd.append(character)

    # Identify the 'missing segment' in the six-segment signals (i.e. the segment that prevents the digit from being number 8)
    for signal in six_segment_signals:
        for character in possible_characters:
            if character not in signal:
                missing_character = character

        # If the missing segment occurs in bd, the missing segment is 'd' and the digit is number 0. The other element in bd is therefore 'b'.
        if missing_character in bd:
            solution['d'] = missing_character
            bd.remove(missing_character)
            solution['b'] = bd[0]
        # If the missing segment occurs in cf, the missing segment is 'c' and the digit is number 6. The other element in cf is therefore 'f'.
        elif missing_character in cf:
            solution['c'] = missing_character
            cf.remove(missing_character)
            solution['f'] = cf[0]
        # If the missing segment doesn't occur in bd or cf, the missing segment is 'e' and the digit is number 9.
        else:
            solution['e'] = missing_character

    # Solve for segment 'g' by comparing to digit number 8
    for character in unique_signals[8]:
        if character not in solution.values():
            solution['g'] = character

    # Turn keys into values and vice versa
    inv_solution = {v: k for k, v in solution.items()}

    convert = []
    for digit in row_out:
        result = ''
        for chr in digit:
            result += inv_solution[chr]
        result = sorted(result)
        result = ''.join(result)
        convert.append(str(signals_to_digits[result]))
    convert = ''.join(convert)
    convert = int(convert)
    output_values.append(convert)

print('Answer 2:', sum(output_values))
