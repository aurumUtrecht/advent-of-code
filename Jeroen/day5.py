import pandas as pd
import numpy as np
import re

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename, "r") as input:
        data = input.read().splitlines()
    return data

input_text = load(5)
data = [re.split(' -> |,', i) for i in input_text]
start_coord = []
end_coord = []
for coords in data:
    x1, y1, x2, y2 = coords
    # if x1 == x2 or y1 == y2:
    start_coord.append([int(x1), int(y1)])
    end_coord.append([int(x2), int(y2)])
print(len(start_coord))

start_coord = np.array(start_coord)
end_coord = np.array(end_coord)

grid = np.zeros(np.max(start_coord, axis=0) + np.array([2, 2]), dtype=int)

for i in range(len(start_coord)):
    x1, y1 = start_coord[i]
    x2, y2 = end_coord[i]

    x_s = np.min([x1, x2])
    y_s = np.min([y1, y2])

    delta_x = np.abs(x2 - x1)
    delta_y = np.abs(y2 - y1)

    if y1 == y2:
        for dx in range(np.abs(delta_x) + 1):
            grid[x_s + dx, y1] += 1
    elif x1 == x2:
        for dy in range(np.abs(delta_y) + 1):
            grid[x1, y_s + dy] += 1
    elif np.abs(delta_x) == np.abs(delta_y):
        if x1 < x2:
            if y1 > y2:
                for d in range(delta_x + 1):
                    grid[x1 + d, y1 - d] += 1
            elif y1 < y2:               
                for d in range(delta_x + 1):
                    grid[x1 + d, y1 + d] += 1

        elif x1 > x2: 
            if y1 < y2:
                for d in range(delta_y + 1):
                    grid[x1 - d, y1 + d] += 1
            elif y1 > y2:
                for d in range(delta_y + 1):
                    grid[x1 - d, y1 - d] += 1
    else:
        print('not 45 degree angle', i)

print(grid)
print("Answer 2:", len(np.where(grid >= 2)[0]))