import pandas as pd
import numpy as np
import csv

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        data = list(csv_reader)
        data = [int(i) for i in data[0]]
    return data

data = load(6)

# Inefficient method
# for i in range(80):
#     new_data = []
#     for j in data:
#         new_timer = j - 1
#         if new_timer == -1:
#             new_timer = 6
#             new_data.append(8)
#         new_data.append(new_timer)
#     data = new_data
# print('Answer 1:', len(data))

data = np.array(data)
unique, counts = np.unique(data, return_counts=True)
totals = dict(zip(unique, counts))
for i in range(256):
    new_totals = {j:0 for j in range(9)}
    for timer, count in totals.items():
        if timer == 0:
            new_totals[6] += count
            new_totals[8] += count
        else:
            new_totals[timer - 1] += count
    totals = new_totals
    if i == 79:
        print('Answer 1:', sum(totals.values()))

print('Answer 2:', sum(totals.values()))