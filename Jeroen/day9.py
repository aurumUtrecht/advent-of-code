import csv
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        data = list(csv_reader)
        data = [i for i in data]
    return data

data = load(9)
data = [item for sublist in data for item in sublist]

x_length = len(data[0])
y_length = len(data)

heatmap = np.zeros([y_length, x_length], dtype=int)
for i in range(len(data)):
    for j in range(len(data[i])):
        character = int(data[i][j])
        heatmap[i][j] = character

lowest_points = []
for i in range(len(heatmap)):
    for j in range(len(heatmap[i])):
        point = heatmap[i, j]
        lowest_point = True
        if i > 0:
            top = heatmap[i - 1, j]
            if top <= point:
                lowest_point = False
        if i < (len(heatmap) - 1):

            bottom = heatmap[i + 1, j]
            if bottom <= point:
                lowest_point = False
        if j > 0:
            left = heatmap[i, j - 1]
            if left <= point:
                lowest_point = False
        if j < (len(heatmap[i]) - 1):
            right = heatmap[i, j + 1]
            if right <= point:
                lowest_point = False
        
        if lowest_point:
            lowest_points.append(point)
            # if point == 9:
            #     1/0
        
print('Answer 1:', sum(lowest_points) + len(lowest_points))
# plt.imshow(heatmap, cmap='hot')
# plt.show()

basin_values = heatmap < 9

labels, nlabels = ndimage.label(basin_values)

# plt.imshow(labels)
# plt.show()

unique, counts = np.unique(labels, return_counts=True)
counts = sorted(counts)
print(counts)
print('Answer 2:', counts[-2] * counts[-3] * counts[-4])