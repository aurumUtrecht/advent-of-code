import numpy as np

a = np.loadtxt('data/day1.txt', dtype=int)
b = np.roll(a, 1)

ans_1 = a - b
ans_1 = ans_1[1:]

ans_1 = ans_1 > 0
print(ans_1.sum())
print('----------------------')

c = np.roll(a, 2)

sum_a = a + b + c
sum_a = sum_a[2:]

ans_2 = sum_a - np.roll(sum_a, 1)
ans_2 = ans_2[1:]

ans_2 = ans_2 > 0
print(ans_2.sum())

