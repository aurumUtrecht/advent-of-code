import numpy as np

def load(day):
    filename = "data/day" + str(day) + ".txt"
    with open(filename, "r") as input:
        data = input.read().splitlines()
    return data

input_text = load(3)
gamma_counts = {}
n_positions = len(input_text[0])

for position in range(n_positions):
    gamma_counts[position] = {
        0: 0,
        1: 0
    }


for entry in input_text:
    for position in range(n_positions):
        bit = int(entry[position])
        gamma_counts[position][bit] += 1 

gamma = ''
epsilon = ''

for position in range(n_positions):
    zeros = gamma_counts[position][0]
    ones = gamma_counts[position][1]

    if zeros >  ones:
        gamma += '0'
        epsilon += '1'
    elif ones > zeros:
        gamma += '1'
        epsilon += '0'
    else:
        print("zeros and ones equal? ", position)

int_gamma = int(gamma, 2)
int_epsilon = int(epsilon, 2)
print('Answer 1: ', int_gamma * int_epsilon)
print('-----------------')

#oxygen
current_list = input_text
for position in range(n_positions):
    new_list = []
    gamma_counts = {
        0: 0,
        1: 0
    }

    for entry in current_list:
        bit = int(entry[position])
        gamma_counts[bit] += 1 

    zeros = gamma_counts[0]
    ones = gamma_counts[1]

    if zeros > ones:
        for entry in current_list:
            if entry[position] == '0':
                new_list.append(entry)
    elif ones > zeros:
        for entry in current_list:
            if entry[position] == '1':
                new_list.append(entry)
    elif ones == zeros:
        for entry in current_list:
            if entry[position] == '1':
                new_list.append(entry)
    else:
        raise AssertionError
    if len(new_list) == 0:
        break
    current_list = new_list

o2 = int(current_list[0], 2)
print('o2', o2)

#co2
current_list = input_text
for position in range(n_positions):
    new_list = []
    gamma_counts = {
        0: 0,
        1: 0
    }

    for entry in current_list:
        bit = int(entry[position])
        gamma_counts[bit] += 1 

    zeros = gamma_counts[0]
    ones = gamma_counts[1]

    if zeros > ones:
        for entry in current_list:
            if entry[position] == '1':
                new_list.append(entry)
    elif ones > zeros:
        for entry in current_list:
            if entry[position] == '0':
                new_list.append(entry)
    elif ones == zeros:
        for entry in current_list:
            if entry[position] == '0':
                new_list.append(entry)
    else:
        raise AssertionError
    if len(new_list) == 0:
        break
    current_list = new_list

co2 = int(current_list[0], 2)
print('co2', co2)

print('Answer 2: ', o2 * co2)