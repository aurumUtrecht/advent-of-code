import numpy as np
from numpy.lib.arraysetops import unique
row = ['acedgfb', 'cdfbe',  'gcdfa', 'fbcad', 'dab', 'cefabd', 'cdfgeb', 'eafb', 'cagedb', 'ab']
out_row = ['cdfeb',  'fcadb',  'cdfeb',  'cdbaf']
possible_characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

solution = dict.fromkeys(possible_characters)

#  8:    
#       aaaa             
#      b    c      
#      b    c
#       dddd             
#      e    f      
#      e    f      
#       gggg

signals_to_digits = {
    'abcefg': 0,
    'cf': 1,
    'acdeg': 2,
    'acdfg': 3,
    'bcdf': 4,
    'abdfg': 5,
    'abdefg': 6,
    'acf': 7,
    'abcdefg': 8,
    'abcdfg': 9
}

unique_signals = {}
six_element_signals = []

for signal in row:
    if len(signal) == 2:
        unique_signals[1] = signal
    if len(signal) == 4:
        unique_signals[4] = signal
    if len(signal) == 3:
        unique_signals[7] = signal
    if len(signal) == 7:
        unique_signals[8] = signal
    if len(signal) == 6:
        six_element_signals.append(signal)

cf = []
for character in unique_signals[7] : # number 7
    if character in unique_signals[1]:
        cf.append(character)
    else:
        solution['a'] = character

bd = []
for character in unique_signals[4]:
    if character not in cf:
        bd.append(character)

for signal in six_element_signals:
    for character in possible_characters:
        if character not in signal:
            missing_character = character

    if missing_character in bd:
        solution['d'] = missing_character
        bd.remove(missing_character)
        solution['b'] = bd[0]
    elif missing_character in cf:
        solution['c'] = missing_character
        cf.remove(missing_character)
        solution['f'] = cf[0]
    else:
        solution['e'] = missing_character

for character in unique_signals[8]:
    if character not in solution.values():
        solution['g'] = character

print(solution)

inv_solution = {v: k for k, v in solution.items()}

convert = []
for digit in out_row:
    result = ''
    for chr in digit:
        result += inv_solution[chr]
    result = sorted(result)
    result = ''.join(result)
    convert.append(str(signals_to_digits[result]))
convert = ''.join(convert)
convert = int(convert)
print(convert)
